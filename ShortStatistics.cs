﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmCQA
{
    public partial class ShortStatistics : Form
    {
        string readerData;
        int[] data;
        public ShortStatistics(string data)
        {
            InitializeComponent();
            readerData = data;
        }
        
        private void ShortStatistics_Load(object sender, EventArgs e)
        {
            if (StatisticsRead(readerData) != true)
            {
                MessageBox.Show("ERRORE");
                this.Close();
            }
            try
            {
                dgShortStatistics.Rows.Clear();
                dgShortStatistics.Columns.Add("Descrizione", "Descrizione");
                dgShortStatistics.Columns.Add("Valore", "Valore");
                dgShortStatistics.Rows.Add("N° di banconote transitate", data[0]);
                dgShortStatistics.Rows.Add("N° di banconote accettate", data[1]);
                dgShortStatistics.Rows.Add("N° di banconote rifiutate", data[2]);
                dgShortStatistics.Rows.Add("N° di banconote ritenute false", data[3]);
                dgShortStatistics.Rows.Add("N° di banconote ritenute sospette", data[4]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima il CLSF check", data[5]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima l' IRD check", data[6]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima il MAG check", data[7]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima il FORMAT check", data[8]);
                dgShortStatistics.Rows.Add("N° di banconote ritenute genuine ma unfit", data[9]);
                dgShortStatistics.Rows.Add("N° di banconote rifiutate per OVERRUN", data[10]);
                dgShortStatistics.Rows.Add("N° di banconote rifiutate perchè il canale bn è disabilitato", data[11]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima l' UVF check", data[12]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per prima l' ExF check", data[13]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per GENERIC error", data[14]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per OCR check", data[15]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per GSA", data[16]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per VBA", data[17]);
                dgShortStatistics.Rows.Add("N° di banconote che falliscono per PRD", data[18]);
            }
            catch { }
            dgShortStatistics.RowHeadersVisible = false;
            dgShortStatistics.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }

        bool StatisticsRead(string myData)
        {

            switch (myData.Length)
            {
                case 64:
                    data = new int[16];
                    break;
                case 128:
                    data = new int[32];
                    break;
                default:
                    return false;
            }
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = StringToInt(myData.Substring(i*4 + 1, 1) + myData.Substring(i*4 + 3, 1));
            }
            return true;
        }
        int StringToInt(string value)
        {
            int result = 0;
            for (int i = 0; i < value.Length; i++)
            {
                switch (value.Substring(i,1))
                {
                    case "A":
                        result += 10;
                        break;
                    case "B":
                        result += 11;
                        break;
                    case "C":
                        result += 12;
                        break;
                    case "D":
                        result += 13;
                        break;
                    case "E":
                        result += 14;
                        break;
                    case "F":
                        result += 15;
                        break;
                    default:
                        result += Convert.ToInt16(value.Substring(i, 1));
                        break;
                }
            }
            return result;
        }
    }


}
