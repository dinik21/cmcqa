﻿namespace CmCQA
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splTitle = new System.Windows.Forms.SplitContainer();
            this.lblTitle = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.lblMain = new System.Windows.Forms.Label();
            this.cboCassNumber = new System.Windows.Forms.ComboBox();
            this.pbMain = new System.Windows.Forms.ProgressBar();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.lblClient = new System.Windows.Forms.Label();
            this.lblPartNumber = new System.Windows.Forms.Label();
            this.cboClient = new System.Windows.Forms.ComboBox();
            this.cboPartNumber = new System.Windows.Forms.ComboBox();
            this.tvErrorLog = new System.Windows.Forms.TreeView();
            this.lblSystem = new System.Windows.Forms.Label();
            this.lblTest = new System.Windows.Forms.Label();
            this.lstComScope = new System.Windows.Forms.ListBox();
            this.lblComScope = new System.Windows.Forms.Label();
            this.dgTest = new System.Windows.Forms.DataGridView();
            this.colVerifica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAtteso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEsito = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analisysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shortStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fillDisplayTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tianmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hitachiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dLCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splTitle)).BeginInit();
            this.splTitle.Panel1.SuspendLayout();
            this.splTitle.Panel2.SuspendLayout();
            this.splTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).BeginInit();
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTest)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splTitle
            // 
            this.splTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splTitle.Location = new System.Drawing.Point(0, 24);
            this.splTitle.Name = "splTitle";
            this.splTitle.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splTitle.Panel1
            // 
            this.splTitle.Panel1.Controls.Add(this.lblTitle);
            this.splTitle.Panel1.Controls.Add(this.picLogo);
            // 
            // splTitle.Panel2
            // 
            this.splTitle.Panel2.Controls.Add(this.splMain);
            this.splTitle.Size = new System.Drawing.Size(784, 537);
            this.splTitle.SplitterDistance = 57;
            this.splTitle.TabIndex = 0;
            this.splTitle.TabStop = false;
            this.splTitle.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(76, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(663, 36);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "CQA (Certified Quality Audithor) for CM Products";
            // 
            // picLogo
            // 
            this.picLogo.BackgroundImage = global::CmCQA.Properties.Resources.logowhite;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picLogo.Location = new System.Drawing.Point(12, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(39, 40);
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // splMain
            // 
            this.splMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splMain.Location = new System.Drawing.Point(0, 0);
            this.splMain.Name = "splMain";
            this.splMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.lblMain);
            this.splMain.Panel1.Controls.Add(this.cboCassNumber);
            this.splMain.Panel1.Controls.Add(this.pbMain);
            this.splMain.Panel1.Controls.Add(this.txtSerialNumber);
            this.splMain.Panel1.Controls.Add(this.lblClient);
            this.splMain.Panel1.Controls.Add(this.lblPartNumber);
            this.splMain.Panel1.Controls.Add(this.cboClient);
            this.splMain.Panel1.Controls.Add(this.cboPartNumber);
            this.splMain.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splMain_Panel1_Paint);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.Controls.Add(this.tvErrorLog);
            this.splMain.Panel2.Controls.Add(this.lblSystem);
            this.splMain.Panel2.Controls.Add(this.lblTest);
            this.splMain.Panel2.Controls.Add(this.lstComScope);
            this.splMain.Panel2.Controls.Add(this.lblComScope);
            this.splMain.Panel2.Controls.Add(this.dgTest);
            this.splMain.Size = new System.Drawing.Size(780, 472);
            this.splMain.SplitterDistance = 189;
            this.splMain.TabIndex = 0;
            this.splMain.TabStop = false;
            this.splMain.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // lblMain
            // 
            this.lblMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lblMain.ForeColor = System.Drawing.Color.White;
            this.lblMain.Location = new System.Drawing.Point(12, 0);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(760, 113);
            this.lblMain.TabIndex = 0;
            this.lblMain.Text = "lblMain";
            // 
            // cboCassNumber
            // 
            this.cboCassNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.cboCassNumber.FormattingEnabled = true;
            this.cboCassNumber.Location = new System.Drawing.Point(326, 116);
            this.cboCassNumber.MaxLength = 2;
            this.cboCassNumber.Name = "cboCassNumber";
            this.cboCassNumber.Size = new System.Drawing.Size(127, 39);
            this.cboCassNumber.TabIndex = 6;
            this.cboCassNumber.Visible = false;
            this.cboCassNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // pbMain
            // 
            this.pbMain.Location = new System.Drawing.Point(12, 170);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(760, 25);
            this.pbMain.TabIndex = 5;
            this.pbMain.Visible = false;
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.txtSerialNumber.Location = new System.Drawing.Point(291, 116);
            this.txtSerialNumber.MaxLength = 12;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(307, 38);
            this.txtSerialNumber.TabIndex = 4;
            this.txtSerialNumber.Visible = false;
            this.txtSerialNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSerialNumber_KeyUp);
            // 
            // lblClient
            // 
            this.lblClient.AutoSize = true;
            this.lblClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lblClient.ForeColor = System.Drawing.Color.White;
            this.lblClient.Location = new System.Drawing.Point(199, 119);
            this.lblClient.Name = "lblClient";
            this.lblClient.Size = new System.Drawing.Size(84, 31);
            this.lblClient.TabIndex = 3;
            this.lblClient.Text = "Client";
            // 
            // lblPartNumber
            // 
            this.lblPartNumber.AutoSize = true;
            this.lblPartNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lblPartNumber.ForeColor = System.Drawing.Color.White;
            this.lblPartNumber.Location = new System.Drawing.Point(12, 119);
            this.lblPartNumber.Name = "lblPartNumber";
            this.lblPartNumber.Size = new System.Drawing.Size(167, 31);
            this.lblPartNumber.TabIndex = 3;
            this.lblPartNumber.Text = "Part Number";
            // 
            // cboClient
            // 
            this.cboClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboClient.Enabled = false;
            this.cboClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.cboClient.FormattingEnabled = true;
            this.cboClient.Location = new System.Drawing.Point(199, 153);
            this.cboClient.Name = "cboClient";
            this.cboClient.Size = new System.Drawing.Size(573, 39);
            this.cboClient.TabIndex = 2;
            this.cboClient.SelectedIndexChanged += new System.EventHandler(this.cboClient_SelectedIndexChanged);
            this.cboClient.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // cboPartNumber
            // 
            this.cboPartNumber.DropDownHeight = 310;
            this.cboPartNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPartNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.cboPartNumber.FormattingEnabled = true;
            this.cboPartNumber.IntegralHeight = false;
            this.cboPartNumber.Location = new System.Drawing.Point(12, 153);
            this.cboPartNumber.Name = "cboPartNumber";
            this.cboPartNumber.Size = new System.Drawing.Size(181, 39);
            this.cboPartNumber.TabIndex = 1;
            this.cboPartNumber.SelectedIndexChanged += new System.EventHandler(this.cboPartNumber_SelectedIndexChanged);
            this.cboPartNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // tvErrorLog
            // 
            this.tvErrorLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvErrorLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvErrorLog.Location = new System.Drawing.Point(290, 40);
            this.tvErrorLog.Name = "tvErrorLog";
            this.tvErrorLog.Size = new System.Drawing.Size(486, 210);
            this.tvErrorLog.TabIndex = 12;
            this.tvErrorLog.Visible = false;
            this.tvErrorLog.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // lblSystem
            // 
            this.lblSystem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSystem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblSystem.ForeColor = System.Drawing.Color.White;
            this.lblSystem.Location = new System.Drawing.Point(126, 253);
            this.lblSystem.Name = "lblSystem";
            this.lblSystem.Size = new System.Drawing.Size(651, 25);
            this.lblSystem.TabIndex = 11;
            this.lblSystem.Text = "4xxxx";
            this.lblSystem.Visible = false;
            // 
            // lblTest
            // 
            this.lblTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTest.AutoSize = true;
            this.lblTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblTest.ForeColor = System.Drawing.Color.White;
            this.lblTest.Location = new System.Drawing.Point(7, 253);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(113, 20);
            this.lblTest.TabIndex = 11;
            this.lblTest.Text = "System in test:";
            this.lblTest.Visible = false;
            // 
            // lstComScope
            // 
            this.lstComScope.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lstComScope.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lstComScope.FormattingEnabled = true;
            this.lstComScope.HorizontalScrollbar = true;
            this.lstComScope.ItemHeight = 16;
            this.lstComScope.Location = new System.Drawing.Point(8, 22);
            this.lstComScope.Name = "lstComScope";
            this.lstComScope.Size = new System.Drawing.Size(277, 228);
            this.lstComScope.TabIndex = 10;
            this.lstComScope.Visible = false;
            this.lstComScope.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // lblComScope
            // 
            this.lblComScope.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblComScope.AutoSize = true;
            this.lblComScope.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.lblComScope.ForeColor = System.Drawing.Color.White;
            this.lblComScope.Location = new System.Drawing.Point(3, -6);
            this.lblComScope.Name = "lblComScope";
            this.lblComScope.Size = new System.Drawing.Size(116, 25);
            this.lblComScope.TabIndex = 9;
            this.lblComScope.Text = "Com Scope";
            this.lblComScope.Visible = false;
            // 
            // dgTest
            // 
            this.dgTest.AllowUserToDeleteRows = false;
            this.dgTest.AllowUserToResizeColumns = false;
            this.dgTest.AllowUserToResizeRows = false;
            this.dgTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colVerifica,
            this.colAtteso,
            this.colValore,
            this.colEsito});
            this.dgTest.EnableHeadersVisualStyles = false;
            this.dgTest.Location = new System.Drawing.Point(291, 22);
            this.dgTest.Name = "dgTest";
            this.dgTest.RowHeadersVisible = false;
            this.dgTest.Size = new System.Drawing.Size(486, 228);
            this.dgTest.TabIndex = 0;
            this.dgTest.Visible = false;
            this.dgTest.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            // 
            // colVerifica
            // 
            this.colVerifica.FillWeight = 125.7868F;
            this.colVerifica.HeaderText = "VERIFICA";
            this.colVerifica.Name = "colVerifica";
            this.colVerifica.ReadOnly = true;
            // 
            // colAtteso
            // 
            this.colAtteso.FillWeight = 62.8934F;
            this.colAtteso.HeaderText = "ATTESO";
            this.colAtteso.Name = "colAtteso";
            this.colAtteso.ReadOnly = true;
            // 
            // colValore
            // 
            this.colValore.FillWeight = 62.8934F;
            this.colValore.HeaderText = "VALORE";
            this.colValore.Name = "colValore";
            this.colValore.ReadOnly = true;
            // 
            // colEsito
            // 
            this.colEsito.FillWeight = 62.8934F;
            this.colEsito.HeaderText = "ESITO";
            this.colEsito.Name = "colEsito";
            this.colEsito.ReadOnly = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "CQA Test";
            this.notifyIcon1.Visible = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionToolStripMenuItem,
            this.analisysToolStripMenuItem,
            this.commandToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // connectionToolStripMenuItem
            // 
            this.connectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem,
            this.disconnectToolStripMenuItem});
            this.connectionToolStripMenuItem.Enabled = false;
            this.connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            this.connectionToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.connectionToolStripMenuItem.Text = "Connection";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click);
            // 
            // disconnectToolStripMenuItem
            // 
            this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
            this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.disconnectToolStripMenuItem.Text = "Disconnect";
            this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
            // 
            // analisysToolStripMenuItem
            // 
            this.analisysToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readerToolStripMenuItem,
            this.errorLogToolStripMenuItem});
            this.analisysToolStripMenuItem.Name = "analisysToolStripMenuItem";
            this.analisysToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.analisysToolStripMenuItem.Text = "Analisys";
            // 
            // readerToolStripMenuItem
            // 
            this.readerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shortStatisticsToolStripMenuItem});
            this.readerToolStripMenuItem.Name = "readerToolStripMenuItem";
            this.readerToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.readerToolStripMenuItem.Text = "Reader";
            // 
            // shortStatisticsToolStripMenuItem
            // 
            this.shortStatisticsToolStripMenuItem.Name = "shortStatisticsToolStripMenuItem";
            this.shortStatisticsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.shortStatisticsToolStripMenuItem.Text = "Short Statistics";
            this.shortStatisticsToolStripMenuItem.Click += new System.EventHandler(this.shortStatisticsToolStripMenuItem_Click);
            // 
            // commandToolStripMenuItem
            // 
            this.commandToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillDisplayTypeToolStripMenuItem,
            this.openLToolStripMenuItem});
            this.commandToolStripMenuItem.Name = "commandToolStripMenuItem";
            this.commandToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.commandToolStripMenuItem.Text = "Command";
            // 
            // fillDisplayTypeToolStripMenuItem
            // 
            this.fillDisplayTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tianmaToolStripMenuItem,
            this.hitachiToolStripMenuItem,
            this.dLCToolStripMenuItem});
            this.fillDisplayTypeToolStripMenuItem.Name = "fillDisplayTypeToolStripMenuItem";
            this.fillDisplayTypeToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.fillDisplayTypeToolStripMenuItem.Text = "Fill Display Type";
            // 
            // tianmaToolStripMenuItem
            // 
            this.tianmaToolStripMenuItem.Name = "tianmaToolStripMenuItem";
            this.tianmaToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.tianmaToolStripMenuItem.Text = "Tianma";
            this.tianmaToolStripMenuItem.Click += new System.EventHandler(this.tianmaToolStripMenuItem_Click);
            // 
            // hitachiToolStripMenuItem
            // 
            this.hitachiToolStripMenuItem.Name = "hitachiToolStripMenuItem";
            this.hitachiToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.hitachiToolStripMenuItem.Text = "Hitachi";
            this.hitachiToolStripMenuItem.Click += new System.EventHandler(this.hitachiToolStripMenuItem_Click);
            // 
            // dLCToolStripMenuItem
            // 
            this.dLCToolStripMenuItem.Name = "dLCToolStripMenuItem";
            this.dLCToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.dLCToolStripMenuItem.Text = "DLC";
            this.dLCToolStripMenuItem.Click += new System.EventHandler(this.dLCToolStripMenuItem_Click);
            // 
            // openLToolStripMenuItem
            // 
            this.openLToolStripMenuItem.Name = "openLToolStripMenuItem";
            this.openLToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.openLToolStripMenuItem.Text = "OpenL";
            this.openLToolStripMenuItem.Click += new System.EventHandler(this.openLToolStripMenuItem_Click);
            // 
            // errorLogToolStripMenuItem
            // 
            this.errorLogToolStripMenuItem.Name = "errorLogToolStripMenuItem";
            this.errorLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.errorLogToolStripMenuItem.Text = "Error Log";
            this.errorLogToolStripMenuItem.Click += new System.EventHandler(this.errorLogToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(22)))), ((int)(((byte)(110)))));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.splTitle);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.splTitle.Panel1.ResumeLayout(false);
            this.splTitle.Panel1.PerformLayout();
            this.splTitle.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splTitle)).EndInit();
            this.splTitle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel1.PerformLayout();
            this.splMain.Panel2.ResumeLayout(false);
            this.splMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splMain)).EndInit();
            this.splMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTest)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splTitle;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.DataGridView dgTest;
        private System.Windows.Forms.Label lblComScope;
        private System.Windows.Forms.ListBox lstComScope;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.Label lblClient;
        private System.Windows.Forms.Label lblPartNumber;
        private System.Windows.Forms.ComboBox cboClient;
        private System.Windows.Forms.ComboBox cboPartNumber;
        private System.Windows.Forms.Label lblMain;
        private System.Windows.Forms.Label lblSystem;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.ProgressBar pbMain;
        private System.Windows.Forms.TreeView tvErrorLog;
        private System.Windows.Forms.ComboBox cboCassNumber;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVerifica;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtteso;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValore;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEsito;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem connectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analisysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shortStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fillDisplayTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tianmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hitachiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dLCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorLogToolStripMenuItem;
    }
}

