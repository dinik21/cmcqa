	unicode true
	; File da includere
	!include "MUI2.nsh"

; Dati applicazione
	!define APPNAME "CM CQA test"
	Name "${APPNAME}"
	!define COMPANYNAME "ARCA Technologies"
	!define VERSIONMAJOR 1
	!define VERSIONMINOR 2
	!define VERSIONBUILD 9

; Impostazione cartelle di sorgente, output ed installazione
	!define INSTFOLDER "C:\ARCA\CmCQA"
	!define SOURCEFOLDER "F:\VisualStudioProject\CmCQA\bin\x86\Debug"
	OutFile "${APPNAME} V${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"
	
; Impostazione Icona installazione/disinstallazione
	!define MUI_ICON "F:\VisualStudioProject\NSISimages\icona.ico"
	!define MUI_UNICON "F:\VisualStudioProject\NSISimages\icona.ico"
	
	!define MUI_ABORTWARNING
	
; Impostazione header (bmp 150x57 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define MUI_HEADERIMAGE_BITMAP "F:\VisualStudioProject\NSISimages\headerlogo.bmp"
	!define MUI_HEADERIMAGE
	
; Impostazione pagina di benvenuto (testo ed immagine bmp 164x314 24bit) GIMP:EsportaComeBmp->'NonScrivereInfoColore'+24bitR8G8B8
	!define txtMessageLine1 "SW guidato per eseguire il CQA dei prodotti ARCA"
	!define txtMessageLine2 ""
	!define txtMessageLine3 ""
	!define MUI_WELCOMEPAGE_TITLE "Installazione CQA"
	!define MUI_WELCOMEPAGE_TEXT "${txtMessageLine1}$\r$\n$\r$\n${txtMessageLine2}$\r$\n$\r$\n${txtMessageLine3}"
	!define MUI_WELCOMEFINISHPAGE_BITMAP "F:\VisualStudioProject\NSISimages\cqawelcome.bmp"
	!insertmacro MUI_PAGE_WELCOME
	
; Impostazione pagina della licenza
	!insertmacro MUI_PAGE_LICENSE "F:\VisualStudioProject\NSISimages\ArcaLicense.txt"
	
; Personalizzazione cartella d'installazione
	;!insertmacro MUI_PAGE_DIRECTORY
	
	
	!insertmacro MUI_PAGE_COMPONENTS
	
; Visualizzazione avanzamento installazione file
	!insertmacro MUI_PAGE_INSTFILES
	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Installazione DLL per il visualizzatore del DBFW"
	;!define MUI_FINISHPAGE_TEXT "Installazione DLL terminata correttamente"
	!define MUI_FINISHPAGE_NOAUTOCLOSE
    !define MUI_FINISHPAGE_RUN
    !define MUI_FINISHPAGE_RUN_NOTCHECKED
	!define MUI_FINISHPAGE_RUN_TEXT "Avvia ${APPNAME}"
	!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchApplication"
	!insertmacro MUI_PAGE_FINISH

	
; Visualizzazione pagin installazione terminata
	;!define MUI_FINISHPAGE_TITLE "Installazione DLL per il visualizzatore del DBFW"
	;!define MUI_FINISHPAGE_TEXT "Installazione DLL terminata correttamente"
	;!insertmacro MUI_PAGE_FINISH
	
; Impostazione conferma di disinstallazione
	!insertmacro MUI_UNPAGE_CONFIRM
	
; Visualizzazione avanzamento disinstallazione file
	!insertmacro MUI_UNPAGE_INSTFILES
		
; Impostazione linguaggio d'installazione
	!insertmacro MUI_LANGUAGE "English"
	
Function LaunchApplication
	ExecShell "" "${INSTFOLDER}\CmCQA.exe"
FunctionEnd
	
Section "CmCQA"
	SetOutPath "${INSTFOLDER}"
	File "${SOURCEFOLDER}\CMCommand.dll"
	File "${SOURCEFOLDER}\CmCQA.exe"
	File "${SOURCEFOLDER}\CMLink.dll"
	File "${SOURCEFOLDER}\CMTrace.dll"
	File "${SOURCEFOLDER}\config.ini"
	File "${SOURCEFOLDER}\italiano.lang"
	File "${SOURCEFOLDER}\TestList.ini"
	File "F:\VisualStudioProject\Comuni\XmlDll.dll"
	
	CreateShortCut "$DESKTOP\${APPNAME}.lnk" "${INSTFOLDER}\CmCQA.exe" ""
	
	WriteUninstaller "${INSTFOLDER}\Uninstall.exe"
SectionEnd



Section "Uninstall"
	RMDir /r "${INSTFOLDER}\*.*"
	RMDir "${INSTFOLDER}"
	Delete "$DESKTOP\${APPNAME}.lnk"
SectionEnd