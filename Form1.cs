﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using CM18tool;
using INI;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;
using XmlDll;

namespace CmCQA
{
    public partial class Form1 : Form
    {
        private const int WM_ACTIVATEAPP = 0x001C;
        private const int WM_USER = 0x0400;
        private const int WM_COMSCOPE_NOTIFY = WM_USER + 1015;
        

        //public CM18tool.Arca Arca;
        public CmDLLs myDll;
        public RS232 Rs232;
        public short hCon = 0;
        public CmDLLs.ConnectionParam CmConnParam;
        public IniFile cfg = new INI.IniFile(Application.StartupPath + "\\config.ini");
        public string serverLogFolder;
        public string logFileName = Application.StartupPath + "\\CmCqaLog.log";
        public IniFile iniMessage;
        //public OdbcConnection myConnection = new OdbcConnection();
        public string myConnectionString;
        public OleDbConnection myConnection = new OleDbConnection();
        public OleDbCommand dbCommand;
        public OleDbDataReader dbReader;
        public DataTable dbDataTable = new DataTable();
        public string sqlCommand;
        public Arca.SSystem mySystem;
        public Arca.SJamErrorLog jamList = new Arca.SJamErrorLog();
        public string[] errorLogLine;
        CmDLLs.STraceDirective traceDirective = new CmDLLs.STraceDirective();
        public string localFolder = Application.StartupPath;
        public string localDBFolder = Application.StartupPath + "\\DB\\";
        public string serverDBFolder;
        public string serverFWFolder;
        public string databaseName;
        public XmlDll.XMLFile xmlLog;
        public const uint ES_CONTINUOS = 0x80000000;
        public const uint ES_SYSTEM_REQUIRED = 0x80000001;
        public const uint ES_DISPLAY_REQUIRED = 0x80000002;
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint SetThreadExecutionState([In] uint esFlags);


        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_COMSCOPE_NOTIFY:
                    Console.WriteLine(Marshal.PtrToStringAnsi(m.WParam));
                    break;
                case WM_ACTIVATEAPP:
                    this.Invalidate();
                    break;
                default:
                    Console.WriteLine(Marshal.PtrToStringAnsi(m.WParam));
                    break;
            }
            base.WndProc(ref m);
        }

        public Form1()
        {
            InitializeComponent();
            InitializaArcaComponents();
            
        }

        public void InitializaArcaComponents()
        {
            myDll = new CmDLLs();
            Rs232 = new RS232();
            CmConnParam = new CmDLLs.ConnectionParam();
            
            string connection = cfg.GetValue("connection", "connection");
            string simplified = cfg.GetValue("connection", "simplified");
            switch (connection)
            {
                case "RS232":
                case "COM":
                case "SERIAL":
                    if (simplified.Substring(0, 1) == "Y" | simplified.Substring(0, 1) == "S")
                    { CmConnParam.ConnectionMode = Convert.ToByte('s'); }
                    else { CmConnParam.ConnectionMode = Convert.ToByte('S'); }
                    CmConnParam.pRsConf.device = cfg.GetValue("rs232", "Port");
                    CmConnParam.pRsConf.baudrate = Convert.ToInt16(cfg.GetValue("rs232", "baudrate"));
                    switch (cfg.GetValue("rs232", "parity").ToUpper())
                    {
                        case "ODD":
                            CmConnParam.pRsConf.parity = 1;
                            break;
                        case "EVEN":
                            CmConnParam.pRsConf.parity = 2;
                            break;
                        default:
                            CmConnParam.pRsConf.parity = 0;
                            break;
                    }
                    CmConnParam.pRsConf.car = Convert.ToInt16(cfg.GetValue("rs232", "databits"));
                    CmConnParam.pRsConf.stop = Convert.ToInt16(cfg.GetValue("rs232", "stopbit"));
                    break;
                case "USB":
                    if (simplified.Substring(0, 1) == "Y" | simplified.Substring(0, 1) == "S")
                    { CmConnParam.ConnectionMode = Convert.ToByte('u'); }
                    else { CmConnParam.ConnectionMode = Convert.ToByte('U'); }
                    break;
                case "LAN":
                    CmConnParam.ConnectionMode = Convert.ToByte('L');
                    break;
            }
            
            myDll.CommandSent += new EventHandler(CmDataSent);
            myDll.CommandReceived += new EventHandler(CmDataReceived);
            iniMessage = new IniFile(Application.StartupPath + "\\" + cfg.GetValue("option", "language"));
            serverLogFolder = cfg.GetValue("option", "testLogFolder");
            
            Arca.iniMessage = iniMessage;
            
            serverDBFolder = cfg.GetValue("option", "DatabaseFolder");
            serverFWFolder = cfg.GetValue("option", "FirmwareFolder");
            databaseName = cfg.GetValue("option", "databaseName");
            myConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + Path.Combine(localDBFolder, databaseName) +
                "; Persist Security Info=True;" + "Jet OLEDB:Database Password=;";
            myConnection.ConnectionString = myConnectionString;
            //MakeLocalDBCopy();
            mySystem = new Arca.SSystem();
            mySystem.cmConfig.bit = new Arca.SBitUnitConfig[16];
            mySystem.cmOption.bit = new Arca.SBitUnitConfig[16];
            mySystem.cmOption1.bit = new Arca.SBitUnitConfig[16];
            mySystem.cmOption2.bit = new Arca.SBitUnitConfig[32];
            for (int i = 0; i<16;i++)
            {
                mySystem.cmConfig.bit[i].value = false;
                mySystem.cmOption.bit[i].value = false;
                mySystem.cmOption1.bit[i].value = false;
            }
            for (int i = 0; i < 32; i++)
            {
                mySystem.cmOption2.bit[i].value = false;
            }

            mySystem.cmConfig.bit[0].address = "0x0001";
            mySystem.cmConfig.bit[0].description = iniMessage.GetValue("unitConfig", "id2"); //View date & time on display
            mySystem.cmConfig.bit[1].address = "0x0002";
            mySystem.cmConfig.bit[1].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[2].address = "0x0004";
            mySystem.cmConfig.bit[2].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[3].address = "0x0008";
            mySystem.cmConfig.bit[3].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[4].address = "0x0010";
            mySystem.cmConfig.bit[4].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[5].address = "0x0020";
            mySystem.cmConfig.bit[5].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[6].address = "0x0040";
            mySystem.cmConfig.bit[6].description = iniMessage.GetValue("unitConfig", "id3"); //Balanced cassette handling
            mySystem.cmConfig.bit[7].address = "0x0080";
            mySystem.cmConfig.bit[7].description = iniMessage.GetValue("unitConfig", "id4"); //Alarm 1 handling
            mySystem.cmConfig.bit[8].address = "0x0100";
            mySystem.cmConfig.bit[8].description = iniMessage.GetValue("unitConfig", "id5"); //Use delay class dispensing
            mySystem.cmConfig.bit[9].address = "0x0200";
            mySystem.cmConfig.bit[9].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[10].address = "0x0400";
            mySystem.cmConfig.bit[10].description = iniMessage.GetValue("unitConfig", "id6"); //Date & time on display in format AM and PM
            mySystem.cmConfig.bit[11].address = "0x0800";
            mySystem.cmConfig.bit[11].description = iniMessage.GetValue("unitConfig", "id7"); //Use UNFIT in SAFE
            mySystem.cmConfig.bit[12].address = "0x1000";
            mySystem.cmConfig.bit[12].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[13].address = "0x2000";
            mySystem.cmConfig.bit[13].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[14].address = "0x4000";
            mySystem.cmConfig.bit[14].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmConfig.bit[15].address = "0x8000";
            mySystem.cmConfig.bit[15].description = iniMessage.GetValue("unitConfig", "id8"); //Enable JOURNAL LOG

            mySystem.cmOption.bit[0].address = "0x0001";
            mySystem.cmOption.bit[0].description = iniMessage.GetValue("unitConfig", "id9"); //Special clean without control
            mySystem.cmOption.bit[1].address = "0x0002";
            mySystem.cmOption.bit[1].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[2].address = "0x0004";
            mySystem.cmOption.bit[2].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[3].address = "0x0008";
            mySystem.cmOption.bit[3].description = iniMessage.GetValue("unitConfig", "id10"); //Special alarm mode
            mySystem.cmOption.bit[4].address = "0x0010";
            mySystem.cmOption.bit[4].description = iniMessage.GetValue("unitConfig", "id11"); //Identify STD/SIMPLIFIED protocol
            mySystem.cmOption.bit[5].address = "0x0020";
            mySystem.cmOption.bit[5].description = iniMessage.GetValue("unitConfig", "id12"); //Execute CLOSE command also in JAM
            mySystem.cmOption.bit[6].address = "0x0040";
            mySystem.cmOption.bit[6].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[7].address = "0x0080";
            mySystem.cmOption.bit[7].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[8].address = "0x0100";
            mySystem.cmOption.bit[8].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[9].address = "0x0200";
            mySystem.cmOption.bit[9].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[10].address = "0x0400";
            mySystem.cmOption.bit[10].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[11].address = "0x0800";
            mySystem.cmOption.bit[11].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[12].address = "0x1000";
            mySystem.cmOption.bit[12].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[13].address = "0x2000";
            mySystem.cmOption.bit[13].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[14].address = "0x4000";
            mySystem.cmOption.bit[14].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption.bit[15].address = "0x8000";
            mySystem.cmOption.bit[15].description = iniMessage.GetValue("unitConfig", "id1"); //reserved

            mySystem.cmOption1.bit[0].address = "0x0001";
            mySystem.cmOption1.bit[0].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[1].address = "0x0002";
            mySystem.cmOption1.bit[1].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[2].address = "0x0004";
            mySystem.cmOption1.bit[2].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[3].address = "0x0008";
            mySystem.cmOption1.bit[3].description = iniMessage.GetValue("unitConfig", "id13"); //Impac 0mm on cassette CR37 in set_cfg
            mySystem.cmOption1.bit[4].address = "0x0010";
            mySystem.cmOption1.bit[4].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[5].address = "0x0020";
            mySystem.cmOption1.bit[5].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[6].address = "0x0040";
            mySystem.cmOption1.bit[6].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[7].address = "0x0080";
            mySystem.cmOption1.bit[7].description = iniMessage.GetValue("unitConfig", "id14"); //Don't remove cass jam when special clean without control ena
            mySystem.cmOption1.bit[8].address = "0x0100";
            mySystem.cmOption1.bit[8].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[9].address = "0x0200";
            mySystem.cmOption1.bit[9].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[10].address = "0x0400";
            mySystem.cmOption1.bit[10].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption1.bit[11].address = "0x0800";
            mySystem.cmOption1.bit[11].description = iniMessage.GetValue("unitConfig", "id15"); //Timeout connect/disconnect LAN
            mySystem.cmOption1.bit[12].address = "0x1000";
            mySystem.cmOption1.bit[12].description = iniMessage.GetValue("unitConfig", "id16"); //Enable time control dispense amount(TCDA)
            mySystem.cmOption1.bit[13].address = "0x2000";
            mySystem.cmOption1.bit[13].description = iniMessage.GetValue("unitConfig", "id33"); //Enable bag Logic FULL handling
            mySystem.cmOption1.bit[14].address = "0x4000";
            mySystem.cmOption1.bit[14].description = iniMessage.GetValue("unitConfig", "id17"); //Enable booking open by external button
            mySystem.cmOption1.bit[15].address = "0x8000";
            mySystem.cmOption1.bit[15].description = iniMessage.GetValue("unitConfig", "id1"); //reserved

            mySystem.cmOption2.bit[0].address = "0x00000001";
            mySystem.cmOption2.bit[0].description = iniMessage.GetValue("unitConfig", "id18"); //Enable 40mm safe
            mySystem.cmOption2.bit[1].address = "0x00000002";
            mySystem.cmOption2.bit[1].description = iniMessage.GetValue("unitConfig", "id19"); //BVU synchronous handling
            mySystem.cmOption2.bit[2].address = "0x00000004";
            mySystem.cmOption2.bit[2].description = iniMessage.GetValue("unitConfig", "id20"); //Banknotes orientation in safe
            mySystem.cmOption2.bit[3].address = "0x00000008";
            mySystem.cmOption2.bit[3].description = iniMessage.GetValue("unitConfig", "id21"); //Special clean using display
            mySystem.cmOption2.bit[4].address = "0x00000010";
            mySystem.cmOption2.bit[4].description = iniMessage.GetValue("unitConfig", "id22"); //Handling cat 2 box
            mySystem.cmOption2.bit[5].address = "0x00000020";
            mySystem.cmOption2.bit[5].description = iniMessage.GetValue("unitConfig", "id34"); //Unit without THSE module
            mySystem.cmOption2.bit[6].address = "0x00000040";
            mySystem.cmOption2.bit[6].description = iniMessage.GetValue("unitConfig", "id23"); //EVO led handling
            mySystem.cmOption2.bit[7].address = "0x00000080";
            mySystem.cmOption2.bit[7].description = iniMessage.GetValue("unitConfig", "id24"); //Separate cat2 and cat3
            mySystem.cmOption2.bit[8].address = "0x00000100";
            mySystem.cmOption2.bit[8].description = iniMessage.GetValue("unitConfig", "id35"); //Alarm board optional
            mySystem.cmOption2.bit[9].address = "0x00000200";
            mySystem.cmOption2.bit[9].description = iniMessage.GetValue("unitConfig", "id25"); //Feed banknotes with input bin free
            mySystem.cmOption2.bit[10].address = "0x00000400";
            mySystem.cmOption2.bit[10].description = iniMessage.GetValue("unitConfig", "id26"); //Different center AU banknotes
            mySystem.cmOption2.bit[11].address = "0x00000800";
            mySystem.cmOption2.bit[11].description = iniMessage.GetValue("unitConfig", "id36"); //New stacker group
            mySystem.cmOption2.bit[12].address = "0x00001000";
            mySystem.cmOption2.bit[12].description = iniMessage.GetValue("unitConfig", "id27"); //Block recovery after power off in cash movements
            mySystem.cmOption2.bit[13].address = "0x00002000";
            mySystem.cmOption2.bit[13].description = iniMessage.GetValue("unitConfig", "id28"); //Shutter handling
            mySystem.cmOption2.bit[14].address = "0x00004000";
            mySystem.cmOption2.bit[14].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[15].address = "0x00008000";
            mySystem.cmOption2.bit[15].description = iniMessage.GetValue("unitConfig", "id29"); //Disable message on display
            mySystem.cmOption2.bit[16].address = "0x00010000";
            mySystem.cmOption2.bit[16].description = iniMessage.GetValue("unitConfig", "id30"); //Alarm2 output handling
            mySystem.cmOption2.bit[17].address = "0x00020000";
            mySystem.cmOption2.bit[17].description = iniMessage.GetValue("unitConfig", "id31"); //Remap feeder jam to missfeeding
            mySystem.cmOption2.bit[18].address = "0x00040000";
            mySystem.cmOption2.bit[18].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[19].address = "0x00080000";
            mySystem.cmOption2.bit[19].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[20].address = "0x00100000";
            mySystem.cmOption2.bit[20].description = iniMessage.GetValue("unitConfig", "id32"); //Virtual desktop environment
            mySystem.cmOption2.bit[21].address = "0x00200000";
            mySystem.cmOption2.bit[21].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[22].address = "0x00400000";
            mySystem.cmOption2.bit[22].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[23].address = "0x00800000";
            mySystem.cmOption2.bit[23].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[24].address = "0x01000000";
            mySystem.cmOption2.bit[24].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[25].address = "0x02000000";
            mySystem.cmOption2.bit[25].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[26].address = "0x04000000";
            mySystem.cmOption2.bit[26].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[27].address = "0x08000000";
            mySystem.cmOption2.bit[27].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[28].address = "0x10000000";
            mySystem.cmOption2.bit[28].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[29].address = "0x20000000";
            mySystem.cmOption2.bit[29].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[30].address = "0x40000000";
            mySystem.cmOption2.bit[30].description = iniMessage.GetValue("unitConfig", "id1"); //reserved
            mySystem.cmOption2.bit[31].address = "0x80000000";
            mySystem.cmOption2.bit[31].description = iniMessage.GetValue("unitConfig", "id1"); //reserved


            mySystem.realtimePhoto.inCenter.name = "ph_incenter";
            mySystem.realtimePhoto.inLeft.name = "ph_inleft";
            mySystem.realtimePhoto.hMax.name = "ph_hmax";
            mySystem.realtimePhoto.feed.name = "ph_feed";
            mySystem.realtimePhoto.c1.name = "ph_c1";
            mySystem.realtimePhoto.shift.name = "ph_shift";
            mySystem.realtimePhoto.inq.name = "ph_inq";
            mySystem.realtimePhoto.count.name = "ph_count";
            mySystem.realtimePhoto.Out.name = "ph_out";
            mySystem.realtimePhoto.c3.name = "ph_c3";
            mySystem.realtimePhoto.c4a.name = "ph_c4a";
            mySystem.realtimePhoto.c4b.name = "ph_c4b";
            mySystem.realtimePhoto.rej.name = "ph_rej";
            mySystem.realtimePhoto.res1.name = "ph_res1";
            mySystem.realtimePhoto.res2.name = "ph_res2";
            mySystem.realtimePhoto.res3.name = "ph_res3";
            mySystem.realtimePhoto.res4.name = "ph_res4";
            mySystem.realtimePhoto.res5.name = "ph_res5";
            mySystem.realtimePhoto.res6.name = "ph_res6";
            mySystem.realtimePhoto.res7.name = "ph_res7";
            mySystem.realtimePhoto.cashLeft.name = "ph_cashleft";
            mySystem.realtimePhoto.cashRight.name = "ph_cashright";
            mySystem.realtimePhoto.thick.name = "ph_thick";
            mySystem.realtimePhoto.finrcyc.name = "ph_finrcyc";
            mySystem.realtimePhoto.res10.name = "ph_res10";
            mySystem.realtimePhoto.res11.name = "ph_res11";
            mySystem.realtimePhoto.res12.name = "ph_res12";
            mySystem.realtimePhoto.res13.name = "ph_res13";
            mySystem.realtimePhoto.res14.name = "ph_res14";
            mySystem.realtimePhoto.res15.name = "ph_res15";
            mySystem.realtimePhoto.res16.name = "ph_res16";
            mySystem.realtimePhoto.res17.name = "ph_res17";
            mySystem.realtimePhoto.res18.name = "ph_res18";
            mySystem.realtimePhoto.res19.name = "ph_res19";
            mySystem.realtimePhoto.res20.name = "ph_res20";
            mySystem.realtimePhoto.res21.name = "ph_res21";
            mySystem.realtimePhoto.res22.name = "ph_res22";
            mySystem.realtimePhoto.res23.name = "ph_res23";
            mySystem.realtimePhoto.res24.name = "ph_res24";
            mySystem.realtimePhoto.res25.name = "ph_res25";

            
        }

        public void CmDataSent(object sender, EventArgs e)
        {
            lstComScope.Items.Add("SND:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }

        public void CmDataReceived(object sender, EventArgs e)
        {
            lstComScope.Items.Add("RCV:" + sender);
            lstComScope.SetSelected(lstComScope.Items.Count - 1, true);
            this.Refresh();
        }
        string singleMessage(int IdMsg)
        {
            return iniMessage.GetValue("messages", "msg" + IdMsg.ToString());
        }

        string ReadDb(string SqlCmd, string fieldName)
        {
            string result = "";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = SqlCmd;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read()==true)
            {
                result = dbReader[fieldName].ToString();
            }
            myConnection.Close();
            return result;
        }

        void FillComboProduct()
        {
            cboPartNumber.Enabled = true;
            cboPartNumber.Items.Clear();
            cboPartNumber.Text = "";
            string mySQL = "SELECT DISTINCT id_prodotto FROM tblman";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = mySQL;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read() == true)
            {
                cboPartNumber.Items.Add(dbReader["id_prodotto"].ToString());
            }
            myConnection.Close();
            cboPartNumber.Focus();
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            if (cfg.GetValue("option","DLLTrace").ToUpper() != "N")
                TraceInizialize();
            MakeLocalDBCopy();
            CopiaLogSuServer();
            FormInizialize();
        }

        void CopiaLogSuServer()
        {
            Arca.WriteMessage(lblMain, 88); //Sposto i file di log ancora presenti nella cartella
            string[] files = Directory.GetFiles(Application.StartupPath);
            foreach (var file in files)
            {
                string filename = Path.GetFileName(file);
                if (filename.Substring(0,4)=="18ET" || filename.Substring(0, 4) == "OM61" || filename.Substring(0, 4) == "18SO" ||
                    filename.Substring(0, 4) == "CM18" || filename.Substring(0, 4) == "CP18" || filename.Substring(0, 4) == "18EV")
                {
                    try
                    {
                        if (File.Exists(serverLogFolder + filename))
                        {
                            filename = Path.GetFileNameWithoutExtension(filename) + "_" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + Path.GetExtension(filename);
                        }
                        Arca.WriteMessage(lblMain, 89, Arca.messageColor, filename); // Sposto il file {0}
                        File.Move(file, serverLogFolder + filename);
                    }
                    catch(Exception ex) { }
                }
            }
        }

        private void TraceInizialize()
        {
            traceDirective = new CmDLLs.STraceDirective();
            traceDirective.infoComp = new CmDLLs.STrComponent[4];
            traceDirective.infoComp[0].level = CmDLLs.EInfoLayer.APPLICATION;
            traceDirective.infoComp[1].level = CmDLLs.EInfoLayer.APPLICATION;
            traceDirective.infoComp[2].level = CmDLLs.EInfoLayer.APPLICATION;
            traceDirective.infoComp[3].level = CmDLLs.EInfoLayer.APPLICATION;
            traceDirective.infoLayer = CmDLLs.ELayer.APPLICATION;
            
            IntPtr myHandle = this.Handle;
            traceDirective.lpComScope = 0; //RISPRISTINARE QUESTO PER IL CORRETTO FUNZIONAMENTO
            //traceDirective.lpComScope.hWin = myHandle;
            string folder = "TraceCmCQA.txt";
            traceDirective.tracePathName = new byte[259]; //259
            for (int i = 0; i < folder.Length; i++)
                traceDirective.tracePathName[i] = Convert.ToByte(Convert.ToChar((folder.Substring(i, 1))));
            traceDirective.trcNum = 30;
            traceDirective.trcSize = 10;
            traceDirective.trcType = CmDLLs.ETcrType.CM_DIMENSION;
            try
            {
                short ret;
                int lastError = 0;
                ret = CmDLLs.SetTraceDirective(traceDirective, ref lastError);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void FillComboClient()
        {
            Arca.keyCodePressed = 0;
            cboClient.Items.Clear();
            string mySQL = "SELECT DISTINCT nome_cliente FROM clienti, tblman WHERE tblman.id_prodotto LIKE'" + cboPartNumber.Text + "' and tblman.id_cliente = clienti.id_cliente";
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = mySQL;
            dbReader = dbCommand.ExecuteReader();
            while (dbReader.Read() == true)
            {
                cboClient.Items.Add(dbReader["nome_cliente"].ToString());
            }
            myConnection.Close();
        }
        private void cboPartNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboClient();
            cboClient.Enabled = true;
            cboClient.Focus();
        }

        private void cboClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            string key = "";
            cboPartNumber.Enabled = false;
            cboClient.Enabled = false;
            mySystem.partNumber = cboPartNumber.Text;
            mySystem.clientName = cboClient.Text;
            mySystem.description = ReadDb("SELECT categoria_prodotto FROM prodotti WHERE id_prodotto = '" + mySystem.partNumber + "'", "categoria_prodotto").ToUpper();
            mySystem.family = mySystem.description.ToUpper();
            mySystem.clientID = ReadDb("SELECT * FROM clienti WHERE nome_cliente = '" + mySystem.clientName + "'", "id_cliente");
            lblSystem.Text = mySystem.description + " - " + mySystem.partNumber + " - " + mySystem.clientName;

            string[] temp;
            temp = mySystem.description.Split(' ');

            if (mySystem.description.IndexOf("CD80")>0)
            {
                mySystem.cd80TrayNumber = Convert.ToInt16(mySystem.description.Substring(mySystem.description.IndexOf("CD80")-1,1));
            }
            if (mySystem.description.IndexOf("CRM")>0)
            {
                mySystem.CRM = 1;
            }
            if (temp.Count()>1)
            {
                mySystem.family = temp[0].ToUpper();
            }

            lblTest.Visible = true;
            lblSystem.Visible = true;
            Arca.WriteMessage(lblMain, 5,Color.White); //Premere INVIO per confermare i dati, ESC per ripetere
            key = Arca.WaitingKey();
            if (key=="ESC")
            {
                cboClient.Items.Clear();
                FillComboProduct();
                lblSystem.Text = "";
                return;
            }
            else
            {
                cboClient.Visible = false;
                cboPartNumber.Visible = false;
                lblClient.Visible = false;
                lblPartNumber.Visible = false;
                dgTest.Visible = false;
            }
            connectionToolStripMenuItem.Enabled = true;
            
            SerialNumber();
        }

        void SerialNumber()
        {
            string tasto;
            txtSerialNumber.Visible = true;
        start:
            txtSerialNumber.Enabled = true;
            txtSerialNumber.Text = "";
            txtSerialNumber.Focus();
            Arca.WriteMessage(lblMain, 7, Color.White); //Inserire il serial number della macchina e premere INVIO
            tasto = Arca.WaitingKey();
            if (tasto == "ESC")
            {
                goto start;
            }
            mySystem.serialNumber = txtSerialNumber.Text;
            lblSystem.Text = mySystem.family + " - " + mySystem.partNumber + " - " + mySystem.clientName + " - " + mySystem.serialNumber;
            presskey:
            Arca.WriteMessage(lblMain, 5, Color.White); //Premere INVIO per confermare i dati, ESC per ripetere
            tasto = Arca.WaitingKey();
            if (tasto == "ESC")
            { goto start; }
            if (tasto != "ENTER")
            { goto presskey; }
            txtSerialNumber.Visible = false;
            lblComScope.Visible = true;
            lstComScope.Visible = true;
            dgTest.Visible = true;
            lblSystem.Visible = true;
            if (CQA()!="PASS")
            {
                Arca.WriteMessage(lblMain, 206, Arca.errorColor); //COLLAUDO FATTITO. premere un tasto per continuare.
            }
            else
            {

                Arca.WriteMessage(lblMain, 38, Color.White); //Collaudo terminato correttamente. Premere un tasto per continuare.
            }
            Arca.WaitingKey();
            InitializaArcaComponents();
            FormInizialize();
        }

        void FormInizialize()
        {
            
            string myIp = string.Empty;
            System.Net.IPAddress[] myIps = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            foreach (System.Net.IPAddress addr in myIps)
            {
                if (addr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    myIp = addr.ToString();
                    break;
                }
            }
            SetThreadExecutionState(ES_CONTINUOS | ES_DISPLAY_REQUIRED);
            this.Text = "CM CQA V" + Application.ProductVersion + "  TeamViewer ID: " + Environment.MachineName + " IpAddress: " + myIp ;
            
            Arca.WriteMessage(lblMain, 2, Color.White); //Selezionare part number e cliente
            cboClient.Visible = true;
            cboPartNumber.Visible = true;
            cboPartNumber.Text = "";
            cboClient.Text = "";
            lblSystem.Text = "";
            lblClient.Visible = true;
            lblPartNumber.Visible = true;
            lstComScope.Items.Clear();
            dgTest.Rows.Clear();
            dgTest.Visible = false;
            lstComScope.Visible = false;
            lblComScope.Visible = false;
            FillComboProduct();
            splMain.SplitterDistance = cboPartNumber.Top + 45;
            dgTest.ClearSelection();
            this.ActiveControl = cboPartNumber;
            
        }
        string LampsButtonTest(int rowIndex)
        {
            inizio:
            Arca.WriteMessage(lblMain, 45,Color.White); //Premere un tasto per avviare la verifica dei tasti operatore
            Arca.WaitingKey();
            Arca.WriteMessage(lblMain, 44,Color.White); //Seguire le istruzione sul display della macchina
            string result = "PASS";
            result = myDll.CMSingleCommand("T,1,6,60,4", 4, 32000);
            if (result != "101" & result != "1")
            {
                dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
                dgTest.Rows[rowIndex].DefaultCellStyle.BackColor = Color.OrangeRed;
                dgTest.Rows[rowIndex].Cells[3].Value = "FAILED";
                goto end;
            }
            //else
            //{
            //    Arca.WriteMessage(lblMain, 207, Color.DarkRed,result); //Risposta: {0}. Premere un tasto per ripetere, ESC per uscire
            //    switch (Arca.WaitingKey())
            //    {
            //        case "ESC":
            //            goto end;
            //        default:
            //            goto inizio;
            //    }

            //}
            dgTest.Rows[rowIndex].Cells["colValore"].Value = "OK";
            //dgTest.Rows[0].Cells[3].Value = "PASS";
            result = "PASS";
        end:
            Arca.LogWrite(logFileName, ",307" + result);
            return result;
        }

        string CMClose(string side = "R")
        {
            start:
            Arca.WriteMessage(lblMain, 39,Color.White, side); //Esecuzione comando CLOSE {0}
            string result = myDll.CMSingleCommand("C,1," + side, 3, 5000);
            if (result != "101" & result != "1")
            {
                if (result == "213" | result=="67")
                {
                    if (side == "R")
                    {
                        side = "L";
                    }
                    else
                    {
                        side = "R";
                    }
                    Arca.WriteMessage(lblMain, 39, Color.White, side); //Esecuzione comando CLOSE {0}
                    result = myDll.CMSingleCommand("C,1," + side, 3, 5000);
                }
                return result;
            }
            return "PASS";
        }
        string Open(string side ="R")
        {
            Arca.WriteMessage(lblMain, 40, Color.White, side); //Esecuzione comando OPEN {0}
            string result = myDll.CMSingleCommand("O,1," + side + ",123456", 3, 5000);
            if (result != "101" & result != "1")
            {
                return result;
            }
            return "PASS";
        }

        //string CashData()
        //{
        //    int[] cash ;
        //    int totalCash=0;
        //    string[] cassette;
        //    int nCassettes = 0;
        //    string result = myDll.CMSingleCommand("G,1,L", 3, 5000);
        //    if (result != "101" & result != "1")
        //    {
        //        return result;
        //    }
            
        //    nCassettes= (myDll.commandSingleAns.Count()-4)/5;
        //    cassette = new string[nCassettes];
        //    cash = new int[nCassettes];
        //    for (int i = 0; i<nCassettes;i++)
        //    {
        //        cassette[i]= myDll.commandSingleAns[(i * 5) + 4];
        //        cash[i] =Convert.ToInt16( myDll.commandSingleAns[(i*5)+5]);
        //        totalCash += cash[i];
        //    }
        //    dgTest.Rows[1].Cells["colValore"].Value = totalCash.ToString();
        //    if (totalCash == 0)
        //    {
        //        return "PASS";
        //    }
        //    return "KO";
        //}

        string FwControl(int rowIndex)
        {
            Arca.WriteMessage(lblMain, 33, Color.White); //Verifica suite installata in corso
            string result = "FAIL";
            result = myDll.CMSingleCommand("T,1,0,84", 4, 5000);
            switch (result)
            {
                case "101":
                case "1":
                    result = "PASS";
                    dgTest.Rows[rowIndex].Cells["colValore"].Value = myDll.commandSingleAns[5];
                    if (dgTest.Rows[rowIndex].Cells["colValore"].Value.ToString().Substring(0,7) != dgTest.Rows[rowIndex].Cells["colAtteso"].Value.ToString())
                        result = "KO";
                    break;
                case "203":
                case "74":
                    Arca.WriteMessage(lblMain, 75,Color.White, dgTest.Rows[rowIndex].Cells["colAtteso"].Value); //Il FW non è in grado di rispondere il codice FFxxxx, verificare tramite display che sia installato il FW {0}. Premere un tasto se corretto, ESC per uscire
                    switch (Arca.WaitingKey())
                    {
                        case "ESC":
                            dgTest.Rows[rowIndex].Cells["colValore"].Value = "WRONG FW";
                            goto end;
                        default:
                            dgTest.Rows[rowIndex].Cells["colValore"].Value = "VERIFIED BY USER";
                            result = "PASS";
                            break;
                    }
                    break;
                default:
                    goto end;
            }
            
        end:
            Arca.LogWrite(logFileName, ",100" + dgTest.Rows[rowIndex].Cells["colValore"].Value.ToString());
            return result;
        }

        string SerialsControl(int rowIndex)
        {
            //string evenSN = cfg.GetValue("CassetteCodes", mySystem.family + "even");
            //string oddSN = cfg.GetValue("CassetteCodes", mySystem.family + "odd");
            //string Cd80EvenSN = cfg.GetValue("CassetteCodes", "CD80even");
            //string Cd80OddSN = cfg.GetValue("CassetteCodes", "CD80odd");
            string[] evenSN = cfg.GetAllValues("CassetteCodes", mySystem.family + "even");
            string[] oddSN = cfg.GetAllValues("CassetteCodes", mySystem.family + "odd");
            string[] Cd80EvenSN = cfg.GetAllValues("CassetteCodes", "CD80even");
            string[] Cd80OddSN = cfg.GetAllValues("CassetteCodes", "CD80odd");

            string result = "PASS";
            Arca.WriteMessage(lblMain, 42); //Get CM Cassette number
            result = myDll.CMSingleCommand("T,1,0,61", 4, 5000);

            if (result != "101" & result != "1")
            {
                goto end;
            }
            mySystem.cassetteNumber = Convert.ToInt16(myDll.commandSingleAns[5]);
            Arca.LogWrite(logFileName, ",00A" + mySystem.cassetteNumber);

            cassetteInstalled:
            Arca.WriteMessage(lblMain,41); //Selezionare il numero di cassetti attualmente montati e premere INVIO
            cboCassNumber.Visible = true;
            cboCassNumber.Items.Clear();
            mySystem.cd80Number = mySystem.cd80TrayNumber * 2;
            for (int i = mySystem.cassetteNumber; i > 4; i -=2)
            {
                cboCassNumber.Items.Add(i);
            };
            lblSystem.Text += " n° " + mySystem.cd80Number.ToString() + " CD80"; 
            cboCassNumber.SelectedIndex = 0;// cboCassNumber.Items.Count-1;
            Arca.WaitingKey("ENTER");
            if (cboCassNumber.Text == "")
                goto cassetteInstalled; 
            mySystem.cassetteInstalled = Convert.ToInt16(cboCassNumber.Text);
            result = myDll.CMSingleCommand("F,1,11," + mySystem.cassetteInstalled.ToString(), 3);
            if (result != "101" & result != "1")
            {
                goto end;
            }
            cboCassNumber.Visible = false;

            Arca.WriteMessage(lblMain, 34, Color.White); //Verifica della presenza di serial number registrati nel sistema (viene verificata la sola presenza).
            
            mySystem.cassettes = new Arca.SCassette[mySystem.cassetteInstalled];
            int idLog = Arca.Hex2Dec("00E");
            for (int i = 0; i < mySystem.cd80Number; i++)
            {
                mySystem.cassettes[i].name = Convert.ToChar(65 + i).ToString();
                mySystem.cassettes[i].serial = myDll.CMSingleCommand("T,1," + Convert.ToChar(65 + i).ToString() + ",13", 8, 5000);
                Arca.LogWrite(logFileName, "," + Arca.Dec2Hex(idLog + i, 3) + mySystem.cassettes[i].serial);
                bool testResult = false;
                if (i % 2 == 0)
                {
                    for (int cd80Even = 0; cd80Even < Cd80EvenSN.Length; cd80Even++)
                    {
                        if (mySystem.cassettes[i].serial.Substring(0, 5) == Cd80EvenSN[cd80Even])
                        {
                            testResult = true;
                            break;
                        }
                    }
                }
                else
                {
                    for (int cd80Odd = 0; cd80Odd < Cd80OddSN.Length; cd80Odd++)
                    {
                        if (mySystem.cassettes[i].serial.Substring(0, 5) == Cd80OddSN[cd80Odd])
                        {
                            testResult = true;
                            break;
                        }
                    }
                }
                if (testResult == false)
                {
                    result = "Cass " + Convert.ToChar(65 + i).ToString() + " KO";
                    goto end;
                }
            }

            for (int i = 0 + mySystem.cd80Number; i < mySystem.cassetteInstalled; i++)
            {
                mySystem.cassettes[i].name = Convert.ToChar(65 + i).ToString();
                mySystem.cassettes[i].serial = myDll.CMSingleCommand("T,1," + Convert.ToChar(65 + i).ToString() + ",13", 8, 5000);
                Arca.LogWrite(logFileName, "," + Arca.Dec2Hex(idLog + i, 3) + mySystem.cassettes[i].serial);
                bool testResult = false;
                if (i % 2 == 0)
                {
                    for (int even = 0; even < evenSN.Length; even++)
                    {
                        if (mySystem.cassettes[i].serial.Substring(0, 5) == evenSN[even])
                        {
                            testResult = true;
                            break;
                        }
                    }
                    
                }
                else
                {
                    for (int odd = 0; odd < oddSN.Length; odd++)
                    {
                        if (mySystem.cassettes[i].serial.Substring(0, 5) == oddSN[odd])
                        {
                            testResult = true;
                            break;
                        }
                    }
                }
                if (testResult == false)
                {
                    result = "Cass " + Convert.ToChar(65 + i).ToString() + " KO";
                    goto end;
                }
            }

            mySystem.serialNumber=myDll.CMSingleCommand("T,1,2,13", 8, 5000);
            mySystem.reader.serial= myDll.CMSingleCommand("T,1,3,13", 8, 5000);
            Arca.LogWrite(logFileName, ",007" + mySystem.reader.serial);

            dgTest.Rows[rowIndex].Cells["colValore"].Value = "OK";
            result = "PASS";
        end:
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            return result;
        }

        string UnitConfigControl(int rowIndex)
        {
            Arca.WriteMessage(lblMain, 35, Color.White); //Verifica UNIT CONFIGURATION in corso
            string tasto;
            string result = "KO";
            string cmConfig, cmOption, cmOption1, cmOption2;
        begin:
            int dbCmConfig = 0;
            int dbCmOption = 0;
            int dbCmOption1 = 0;
            int dbCmOption2 = 0;
            int idUnitConfig = 0;

            //CMConfig
            cmConfig = myDll.CMSingleCommand("T,1,0,15", 5, 5000);
            Arca.LogWrite(logFileName, ",02D" + cmConfig);

            //CMOption
            cmOption = myDll.CMSingleCommand("T,1,0,28", 5, 5000);
            Arca.LogWrite(logFileName, ",02E" + cmOption);

            //CMOption ONE
            cmOption1 = myDll.CMSingleCommand("T,1,0,32", 5, 5000);
            Arca.LogWrite(logFileName, ",02F" + cmOption1);

            //CMOption TWO
            cmOption2 = myDll.CMSingleCommand("T,1,0,49", 5, 5000);
            Arca.LogWrite(logFileName, ",030" + cmOption2);

            idUnitConfig =Convert.ToInt16(ReadDb("SELECT id_nome_unit_config FROM tblman WHERE id_prodotto ='" + mySystem.partNumber + "' AND id_cliente = " + mySystem.clientID + " AND id_modulo = 'fw_prodotto'", "id_nome_unit_config"));
            if (mySystem.partNumber == "43664" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543" || mySystem.partNumber == "43543-TEMP")
                idUnitConfig = 6;
            Arca.LogWrite(logFileName, ",031" + idUnitConfig.ToString());
            FillDbUnitConfig(idUnitConfig);
            dbCmConfig = 0;
            dbCmOption = 0;
            dbCmOption1 = 0;
            dbCmOption2 = 0;

            for (int i = 0; i < mySystem.cmConfig.bit.Count(); i++)
            {
                dbCmConfig += (int)Math.Pow(2, (i)) * Convert.ToInt32(mySystem.cmConfig.bit[i].value);
            }

            for (int i = 0; i < mySystem.cmOption.bit.Count(); i++)
            {
                dbCmOption += (int)Math.Pow(2, (i)) * Convert.ToInt32(mySystem.cmOption.bit[i].value);
            }

            for (int i = 0; i < mySystem.cmOption1.bit.Count(); i++)
            {
                dbCmOption1 += (int)Math.Pow(2, (i)) * Convert.ToInt32(mySystem.cmOption1.bit[i].value);
            }

            for (int i = 0; i < mySystem.cmOption2.bit.Count(); i++)
            {
                dbCmOption2 += (int)Math.Pow(2, (i)) * Convert.ToInt32(mySystem.cmOption2.bit[i].value);
            }

        cmconfig:
            Arca.WriteMessage(lblMain, 37, Color.White,"CM CONFIG"); //Confronto UNIT CONFIGURATION ({0}) in macchina con quello nel DBFW
            if (cmConfig != Arca.Dec2Hex(dbCmConfig,4))
            {
                Arca.WriteMessage(lblMain, 200, Arca.errorColor, cmConfig, Arca.Dec2Hex(dbCmConfig)); //Unit configuration (CM Config) non coerente con il database. risposta {0} invece di {1}. Premere un tasto per ripetere, D per i dettagli od ESC per uscire.
                tasto= Arca.WaitingKey();
                if (tasto=="ESC") { goto end; }
                if (tasto=="D")
                {
                    string binCmConfig = Arca.Hex2Bin(cmConfig);
                    string message = "";
                    for (int i=0;i<16;i++)
                    {
                        if((mySystem.cmConfig.bit[i].value==false & binCmConfig.Substring(15-i,1)=="1") | (mySystem.cmConfig.bit[i].value == true & binCmConfig.Substring(15-i, 1) == "0"))
                        {
                            message += mySystem.cmConfig.bit[i].address + " - " + mySystem.cmConfig.bit[i].description + " - " + mySystem.cmConfig.bit[i].value + (char)13;
                        }
                    }
                    MessageBox.Show(message,"CORRECT SETTINGS");
                    goto cmconfig;
                }
                goto begin;
            }

            cmoption:
            Arca.WriteMessage(lblMain, 37, Color.White, "CM OPTION CONFIG"); //Confronto UNIT CONFIGURATION ({0}) in macchina con quello nel DBFW
            if (cmOption != Arca.Dec2Hex(dbCmOption,4))
            {
                Arca.WriteMessage(lblMain, 201, Arca.errorColor, cmOption, Arca.Dec2Hex(dbCmOption)); //Unit configuration (CM Option Config) non coerente con il database. risposta {0} invece di {1}. Premere un tasto per ripetere, D per i dettagli od ESC per uscire.
                tasto = Arca.WaitingKey();
                if (tasto == "ESC") { goto end; }
                if (tasto == "D")
                {
                    string binCmOption = Arca.Hex2Bin(cmOption);
                    string message = "";
                    for (int i = 0; i < 16; i++)
                    {
                        if ((mySystem.cmOption.bit[15 - i].value == false & binCmOption.Substring(15-i, 1) == "1") | (mySystem.cmOption.bit[15 - i].value == true & binCmOption.Substring(15-i, 1) == "0"))
                        {
                            message += mySystem.cmOption.bit[i].address + " - " + mySystem.cmOption.bit[i].description + " - " + mySystem.cmOption.bit[i].value + (char)13;
                        }
                    }
                    MessageBox.Show(message, "CORRECT SETTINGS");
                    goto cmoption;
                }
                goto begin;
            }

            cmoption1:
            Arca.WriteMessage(lblMain, 37, Color.White, "CM OPTION ONE CONFIG"); //Confronto UNIT CONFIGURATION ({0}) in macchina con quello nel DBFW
            if (cmOption1 != Arca.Dec2Hex(dbCmOption1,4))
            {
                Arca.WriteMessage(lblMain, 202, Arca.errorColor, cmOption1, Arca.Dec2Hex(dbCmOption1)); //Unit configuration (CM Option One Config) non coerente con il database. risposta {0} invece di {1}. Premere un tasto per ripetere, D per i dettagli od ESC per uscire.
                tasto = Arca.WaitingKey();
                if (tasto == "ESC") { goto end; }
                if (tasto == "D")
                {
                    string binCmOption1 = Arca.Hex2Bin(cmOption1);
                    string message = "";
                    for (int i = 0; i < 16; i++)
                    {
                        if ((mySystem.cmOption1.bit[15 - i].value == false & binCmOption1.Substring(15-i, 1) == "1") | (mySystem.cmOption1.bit[15 - i].value == true & binCmOption1.Substring(15-i, 1) == "0"))
                        {
                            message += mySystem.cmOption1.bit[i].address + " - " + mySystem.cmOption1.bit[i].description + " - " + mySystem.cmOption1.bit[i].value + (char)13;
                        }
                    }
                    MessageBox.Show(message, "CORRECT SETTINGS");
                    goto cmoption1;
                }
                goto begin;
            }

            cmoption2:
            Arca.WriteMessage(lblMain, 37, Color.White, "CM OPTION TWO CONFIG"); //Confronto UNIT CONFIGURATION ({0}) in macchina con quello nel DBFW
            if (cmOption2 != Arca.Dec2Hex(dbCmOption2,8))
            {
                Arca.WriteMessage(lblMain, 203, Arca.errorColor, cmOption2, Arca.Dec2Hex(dbCmOption2)); //Unit configuration (CM Option Two Config) non coerente con il database. risposta {0} invece di {1}. Premere un tasto per ripetere, D per i dettagli od ESC per uscire.
                tasto = Arca.WaitingKey();
                if (tasto == "ESC") { goto end; }
                if (tasto == "D")
                {
                    string binCmOption2 = Arca.Hex2Bin(cmOption2);
                    string message = "";
                    for (int i = 0; i < 32; i++)
                    {
                        if ((mySystem.cmOption2.bit[i].value == false & binCmOption2.Substring(31-i, 1) == "1") | (mySystem.cmOption2.bit[i].value == true & binCmOption2.Substring(31-i, 1) == "0"))
                        {
                            message += mySystem.cmOption2.bit[i].address + " - " + mySystem.cmOption2.bit[i].description + " - " + mySystem.cmOption2.bit[i].value + (char)13;
                        }
                    }
                    MessageBox.Show(message, "CORRECT SETTINGS");
                    goto cmoption2;
                }
                goto begin;
            }

            result = "PASS";
        end:
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            return result;
        }

        void FillDbUnitConfig(int idUnitConfig)
        {
            Arca.WriteMessage(lblMain, 36, Color.White); //Lettura unit configuration da DBFW
            mySystem.cmConfig.bit[0].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "View date & time on display"));
            mySystem.cmConfig.bit[6].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Balance cassette handling"));
            mySystem.cmConfig.bit[7].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Alarm1 handling"));
            mySystem.cmConfig.bit[8].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Use delay class dispensing"));
            mySystem.cmConfig.bit[10].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Data & time on display in format AM and PM"));
            mySystem.cmConfig.bit[11].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Use Unfit in safe"));
            mySystem.cmConfig.bit[15].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Enable journal log"));

            mySystem.cmOption.bit[0].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Special clean without control"));
            mySystem.cmOption.bit[3].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Special alarm mode"));
            mySystem.cmOption.bit[4].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Identify STD/SIMPLIFIED protocol"));
            mySystem.cmOption.bit[5].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Execute close command also in JAM"));

            mySystem.cmOption1.bit[3].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Impac  0mm on cassette CR37 in set_cfg"));
            mySystem.cmOption1.bit[7].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Don't remove cass jam when special clean without control ena"));
            mySystem.cmOption1.bit[10].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "SSL protocol (LAN)"));
            mySystem.cmOption1.bit[11].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Timeout connect/disconnect LAN"));
            mySystem.cmOption1.bit[12].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Enable time control dispence amount (TCDA)"));
            mySystem.cmOption1.bit[14].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Enable booking open by external button"));

            mySystem.cmOption2.bit[0].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Safe cen40 bit"));
            mySystem.cmOption2.bit[1].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "BVU synchronous handling"));
            mySystem.cmOption2.bit[2].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Handling facing in safe_bit"));
            mySystem.cmOption2.bit[3].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Special clean using display"));
            mySystem.cmOption2.bit[4].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Handling cat 2 box"));
            mySystem.cmOption2.bit[5].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "CM18b without THSE module"));
            mySystem.cmOption2.bit[6].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "handling led"));
            mySystem.cmOption2.bit[7].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Use cat 3 in safe"));
            mySystem.cmOption2.bit[8].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Cash Control Licence"));
            mySystem.cmOption2.bit[9].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Feed banknotes with input bin free"));
            mySystem.cmOption2.bit[10].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Different center AU banknotes"));
            mySystem.cmOption2.bit[11].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "New Stacker"));
            mySystem.cmOption2.bit[12].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Block recovery after power off in cash movements"));
            mySystem.cmOption2.bit[13].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Shutter handling"));
            mySystem.cmOption2.bit[14].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_15"));
            mySystem.cmOption2.bit[15].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Disable message on display"));
            mySystem.cmOption2.bit[16].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Alarm 2 output handling"));
            mySystem.cmOption2.bit[17].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "Remap feederjam to missfeeding"));
            mySystem.cmOption2.bit[18].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_19"));
            mySystem.cmOption2.bit[19].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_20"));
            mySystem.cmOption2.bit[20].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_21"));
            mySystem.cmOption2.bit[21].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_22"));
            mySystem.cmOption2.bit[22].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_23"));
            mySystem.cmOption2.bit[23].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_24"));
            mySystem.cmOption2.bit[24].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_25"));
            mySystem.cmOption2.bit[25].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_26"));
            mySystem.cmOption2.bit[26].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_27"));
            mySystem.cmOption2.bit[27].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_28"));
            mySystem.cmOption2.bit[28].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_29"));
            mySystem.cmOption2.bit[29].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_30"));
            mySystem.cmOption2.bit[30].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_31"));
            mySystem.cmOption2.bit[31].value = Convert.ToBoolean(ReadDb("SELECT * FROM tbl_unit_config WHERE id_nome_unit_config = " + idUnitConfig, "NU_32"));
        }

        string DateTimeControl(int rowIndex)
        {
        start:
            string systemDateTime = "";
            string pcDateTime = "";
            string result = "PASS";
            int second, minute, hour, day, month, year;
            int pcMinute, pcHour, pcDay, pcMonth, pcYear;
            string[] date = DateTime.Now.ToShortDateString().Split('/');
            pcDay =Convert.ToInt16( date[0]);
            pcMonth = Convert.ToInt16(date[1]);
            pcYear = Convert.ToInt16(date[2].Substring(2,2));
            string[] time = DateTime.Now.ToShortTimeString().Split(':');
            pcHour = Convert.ToInt16(time[0]);
            pcMinute = Convert.ToInt16(time[1]);
            result = myDll.CMSingleCommand("T,1,0,10", 4, 5000);
            if (result != "101" & result != "1")
            {
                result = "KO";
                goto end;
            }
            result = "PASS";
            second = Convert.ToInt16(myDll.commandSingleAns[6]);
            minute = Convert.ToInt16(myDll.commandSingleAns[7]);
            hour = Convert.ToInt16(myDll.commandSingleAns[8]);
            day = Convert.ToInt16(myDll.commandSingleAns[10]);
            month = Convert.ToInt16(myDll.commandSingleAns[11]);
            year = Convert.ToInt16(myDll.commandSingleAns[12]);
            systemDateTime = day.ToString("00") + "/" + month.ToString() + "/" + year.ToString() +" - "+ hour.ToString("00") + ":" + minute.ToString("00");
            pcDateTime = pcDay.ToString() + "/" + pcMonth.ToString() + "/" + pcYear.ToString() + " - " + pcHour.ToString() + ":" + pcMinute.ToString();
            if (pcYear!=year | pcMonth!=month | Math.Abs(pcHour - hour) > 1 | Math.Abs(pcMinute - minute) > 75)
            {
                Arca.WriteMessage(lblMain, 204, Arca.errorColor,systemDateTime,DateTime.Now.ToString()); //Impostazione di Data e Ora diversa da quella del PC in uso. Verificare le impostazioni di PC e Macchina e premere un tasato per ripetere o ESC per uscire.
                if(Arca.WaitingKey() == "ESC")
                {
                    result = "KO";
                    goto end;
                }
                goto start;
            }
            
        end:
            dgTest.Rows[rowIndex].Cells["colValore"].Value = systemDateTime;
            Arca.LogWrite(logFileName, ",032" + systemDateTime);
            return result;
        }

        string ErrorLogRead(int rowIndex, string parameter = "", bool saveNumberErrors = true)
        {

            string result = "PASS";
            if (parameter != "" && (mySystem.partNumber == "43664" || mySystem.partNumber == "43543" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543-TEMP"))
                return result;
            Arca.WriteMessage(lblMain, 31, Color.White); //Lettura error log in corso
            tvErrorLog.Visible = true;
            pbMain.Visible = true;
            pbMain.Minimum = 1;
            int lastErrorLog = 0;
            string[] errorLogLine;
            result = myDll.CMSingleCommand("H,1,UUUU", 2, 5000);
            if (result != "101" & result != "1")
            {
                return result;
            }

            lastErrorLog = Convert.ToInt16(myDll.commandSingleAns[3]);
            pbMain.Maximum = lastErrorLog+1;
            errorLogLine = new string[lastErrorLog+1];
            //List<Arca.SJamDetail> jamDetail = new List<Arca.SJamDetail>();
            jamList.jamDetail = new List<Arca.SJamDetail>();
            jamList.totalErrors = 0;
            mySystem.errorLog = new Arca.SErrorLog[lastErrorLog + 1];
            result = myDll.CMSingleCommand("H,1,0000", 2, 5000);
            if (result != "101" & result != "1")
            {
                return result;
            }
            mySystem.errorLog[0].life=Convert.ToInt32( myDll.commandSingleAns[4]);
            TreeNode nodeJam = new TreeNode("JAM");
            TreeNode newNode;
            nodeJam.Name = "JAM";
            tvErrorLog.Nodes.Add(nodeJam);

            for (int i = 1; i <= lastErrorLog; i++)
            {
                pbMain.Value = i;
                result = myDll.CMSingleCommand("H,1," + String.Format("{0:0000}", i), 2, 6000);
                if (result != "101" & result != "1")
                {
                    return result;
                }
                this.Text = "lastErrorLog=" + lastErrorLog.ToString() + " - i=" + i.ToString() + " - commandSingAns.dimension= " + myDll.commandSingleAns.Length;
                try
                {
                    mySystem.errorLog[i].line = myDll.commandAns;
                    mySystem.errorLog[i].recordNumber = Convert.ToInt32(myDll.commandSingleAns[3]);
                    mySystem.errorLog[i].life = Convert.ToInt32(myDll.commandSingleAns[4]);
                    mySystem.errorLog[i].opSide = myDll.commandSingleAns[5];
                    mySystem.errorLog[i].opId = myDll.commandSingleAns[6];
                    mySystem.errorLog[i].replyCode = myDll.commandSingleAns[7];
                    //if (mySystem.errorLog[i].replyCode != "581" && mySystem.errorLog[i].replyCode != "580")
                    //{
                    mySystem.errorLog[i].F1Status = myDll.commandSingleAns[8];
                    mySystem.errorLog[i].F2Status = myDll.commandSingleAns[9];
                    mySystem.errorLog[i].F3Status = myDll.commandSingleAns[10];
                    mySystem.errorLog[i].F4Status = myDll.commandSingleAns[11];
                    mySystem.errorLog[i].FAStatus = myDll.commandSingleAns[12];
                    mySystem.errorLog[i].FBStatus = myDll.commandSingleAns[13];
                    mySystem.errorLog[i].FCStatus = myDll.commandSingleAns[14];
                    mySystem.errorLog[i].FDStatus = myDll.commandSingleAns[15];
                    mySystem.errorLog[i].FEStatus = myDll.commandSingleAns[16];
                    mySystem.errorLog[i].FFStatus = myDll.commandSingleAns[17];
                    mySystem.errorLog[i].FGStatus = myDll.commandSingleAns[18];
                    mySystem.errorLog[i].FHStatus = myDll.commandSingleAns[19];
                    mySystem.errorLog[i].FIStatus = myDll.commandSingleAns[20];
                    mySystem.errorLog[i].FJStatus = myDll.commandSingleAns[21];

                    if (mySystem.family != "CM18" && mySystem.family != "CM18SOLO" && mySystem.family != "CM20")
                    {
                        mySystem.errorLog[i].FKStatus = myDll.commandSingleAns[22];
                        mySystem.errorLog[i].FLStatus = myDll.commandSingleAns[23];
                        mySystem.errorLog[i].FMStatus = myDll.commandSingleAns[24];
                        mySystem.errorLog[i].FNStatus = myDll.commandSingleAns[25];
                        mySystem.errorLog[i].FOStatus = myDll.commandSingleAns[26];
                        mySystem.errorLog[i].FPStatus = myDll.commandSingleAns[27];
                    }
                }
                catch (System.IndexOutOfRangeException ex)
                {
                    this.Text = ex.Message;
                }
                //}
                try
                {
                    if (mySystem.errorLog[i].opSide != "+" && mySystem.errorLog[i].opSide != "-" && mySystem.errorLog[i].opSide != null)
                    {
                        if ((mySystem.errorLog[i].replyCode.Length == 3 && mySystem.errorLog[i].replyCode != "401") && (mySystem.errorLog[i].replyCode.Substring(0,1)=="3" || mySystem.errorLog[i].replyCode.Substring(0,1) == "4" ) || ((mySystem.errorLog[i].replyCode.Length==2) && mySystem.errorLog[i].replyCode != "15" && mySystem.errorLog[i].replyCode != "14" && mySystem.errorLog[i].replyCode != "13" && mySystem.errorLog[i].replyCode != "12" && mySystem.errorLog[i].replyCode != "11" && mySystem.errorLog[i].replyCode != "10"))
                        {
                            Arca.SJamDetail tempJamDetail = new Arca.SJamDetail();

                            jamList.totalErrors += 1;
                            newNode = new TreeNode(mySystem.errorLog[i].replyCode);
                            newNode.Name = mySystem.errorLog[i].replyCode;
                            //tvErrorLog.Nodes[nodeJam.Name].Text = string.Format("JAM ({0})", jamList.totalErrors);
                            tvErrorLog.Nodes[nodeJam.Name].Text = string.Format("JAM");
                            if (!tvErrorLog.Nodes[nodeJam.Name].Nodes.ContainsKey("reply: " + mySystem.errorLog[i].replyCode))
                            {
                                tvErrorLog.Nodes[nodeJam.Name].Nodes.Add(newNode);
                                tvErrorLog.Nodes[nodeJam.Name].Nodes[newNode.Name].Text = "reply: " + mySystem.errorLog[i].replyCode;
                            }
                            
                            tvErrorLog.Nodes[nodeJam.Name].Nodes[newNode.Name].Nodes.Add("line " + mySystem.errorLog[i].recordNumber + ", reply:" + mySystem.errorLog[i].replyCode + " Feeder:" + mySystem.errorLog[i].F1Status.Substring(1) + " Controller:" + mySystem.errorLog[i].F2Status.Substring(1) + " Safe:" + mySystem.errorLog[i].F3Status.Substring(1) + " Reader:" + mySystem.errorLog[i].F4Status.Substring(1));
                            tempJamDetail.line = mySystem.errorLog[i].recordNumber;

                            if (mySystem.errorLog[i].F1Status.Substring(1, 2) != "40")
                            {
                                tempJamDetail.module = "Feeder";
                                jamList.feedErrors += 1;
                            }
                            else if (mySystem.errorLog[i].F2Status.Substring(1, 2) != "40")
                            {
                                tempJamDetail.module = "Controller";
                                jamList.controllerErrors += 1;
                            }
                            else if (mySystem.errorLog[i].F3Status.Substring(1, 2) != "40")
                            {
                                tempJamDetail.module = "Reader";
                                jamList.readerErrors += 1;
                            }
                            else if (mySystem.errorLog[i].F4Status.Substring(1, 2) != "40")
                            {
                                tempJamDetail.module = "Safe";
                                jamList.safeErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FAStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FAStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FAStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette A";
                                jamList.cassetteAErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FBStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FBStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FBStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette B";
                                jamList.cassetteBErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FCStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FCStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FCStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette C";
                                jamList.cassetteCErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FDStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FDStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FDStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette D";
                                jamList.cassetteDErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FEStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FEStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FEStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette E";
                                jamList.cassetteEErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FFStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FFStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FFStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette F";
                                jamList.cassetteFErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FGStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FGStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FGStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette G";
                                jamList.cassetteGErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FHStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FHStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FHStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette H";
                                jamList.cassetteHErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FIStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FIStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FIStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette I";
                                jamList.cassetteIErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FJStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FJStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FJStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette J";
                                jamList.cassetteJErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FKStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FKStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FKStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette K";
                                jamList.cassetteKErrors += 1;
                            }
                            else if (mySystem.errorLog[i].FLStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FLStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FLStatus.Substring(1, 2) != "00")
                            {
                                tempJamDetail.module = "Cassette L";
                                jamList.cassetteLErrors += 1;
                            }

                            jamList.jamDetail.Add(tempJamDetail);
                        }
                    }
                    //if ((mySystem.errorLog[i].replyCode.Substring(0, 1) == "3" | mySystem.errorLog[i].replyCode.Substring(0, 1) == "4") & mySystem.errorLog[i].replyCode.Length == 3)
                    //{

                    //    Arca.SJamDetail tempJamDetail = new Arca.SJamDetail();

                    //    jamList.totalErrors += 1;
                    //    newNode = new TreeNode(mySystem.errorLog[i].replyCode);
                    //    newNode.Name = mySystem.errorLog[i].replyCode;
                    //    tvErrorLog.Nodes[nodeJam.Name].Text = string.Format("JAM ({0})", jamList.totalErrors);
                    //    tvErrorLog.Nodes[nodeJam.Name].Nodes.Add(newNode);
                    //    tvErrorLog.Nodes[nodeJam.Name].Nodes[newNode.Name].Text = "reply: " + mySystem.errorLog[i].replyCode;
                    //    tvErrorLog.Nodes[nodeJam.Name].Nodes[newNode.Name].Nodes.Add("line " + mySystem.errorLog[i].recordNumber + ", reply:" + mySystem.errorLog[i].replyCode + " Feeder:" + mySystem.errorLog[i].F1Status.Substring(1) + " Controller:" + mySystem.errorLog[i].F2Status.Substring(1) + " Safe:" + mySystem.errorLog[i].F3Status.Substring(1) + " Reader:" + mySystem.errorLog[i].F4Status.Substring(1));
                    //    tempJamDetail.line = mySystem.errorLog[i].recordNumber;

                    //    if (mySystem.errorLog[i].F1Status.Substring(1, 2) != "40")
                    //    {
                    //        tempJamDetail.module = "Feeder";
                    //        jamList.feedErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].F2Status.Substring(1, 2) != "40")
                    //    {
                    //        tempJamDetail.module = "Controller";
                    //        jamList.controllerErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].F3Status.Substring(1, 2) != "40")
                    //    {
                    //        tempJamDetail.module = "Reader";
                    //        jamList.readerErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].F4Status.Substring(1, 2) != "40")
                    //    {
                    //        tempJamDetail.module = "Safe";
                    //        jamList.safeErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FAStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FAStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FAStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette A";
                    //        jamList.cassetteAErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FBStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FBStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FBStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette B";
                    //        jamList.cassetteBErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FCStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FCStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FCStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette C";
                    //        jamList.cassetteCErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FDStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FDStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FDStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette D";
                    //        jamList.cassetteDErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FEStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FEStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FEStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette E";
                    //        jamList.cassetteEErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FFStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FFStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FFStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette F";
                    //        jamList.cassetteFErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FGStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FGStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FGStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette G";
                    //        jamList.cassetteGErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FHStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FHStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FHStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette H";
                    //        jamList.cassetteHErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FIStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FIStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FIStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette I";
                    //        jamList.cassetteIErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FJStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FJStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FJStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette J";
                    //        jamList.cassetteJErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FKStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FKStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FKStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette K";
                    //        jamList.cassetteKErrors += 1;
                    //    }
                    //    else if (mySystem.errorLog[i].FLStatus.Substring(1, 2) != "40" & mySystem.errorLog[i].FLStatus.Substring(1, 2) != "05" & mySystem.errorLog[i].FLStatus.Substring(1, 2) != "00")
                    //    {
                    //        tempJamDetail.module = "Cassette L";
                    //        jamList.cassetteLErrors += 1;
                    //    }

                    //    jamList.jamDetail.Add(tempJamDetail);

                    //}
                }
                catch (Exception ex)
                {
                    this.Text = ex.Message;
                }
            }
            if (saveNumberErrors == true)
                Arca.LogWrite(logFileName, ",033" + jamList.totalErrors.ToString());

            result = "PASS";
            if (jamList.totalErrors > 0)
            {
                Arca.WriteMessage(lblMain, 8, Color.White); //Riportare eventuali jam nel database di produzione. Premere un tasto per continuare, ESC per uscire
                string tasto = Arca.WaitingKey();
                if (tasto == "ESC")
                {
                    result = "KO";
                }
            }
            pbMain.Visible = false;
            tvErrorLog.Nodes.Clear();
            tvErrorLog.Visible = false;
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            return result;
        }

        void SystemInformationUpdate()
        {
            Arca.WriteMessage(lblMain, 51, Color.White);//Acquisizione informazioni sulla macchina in corso
            myDll.CMSingleCommand("T,1,0,88");
            if (myDll.commandSingleAns.Length > 5)
            {
                mySystem.protInterface = myDll.commandSingleAns[5];
                mySystem.family = myDll.commandSingleAns[7].ToUpper();
                mySystem.cd80Number = Convert.ToInt16(myDll.commandSingleAns[12]);
                mySystem.protCassetteNumber = Convert.ToInt16(myDll.commandSingleAns[8]);
            }

            //Compatibility
            myDll.CMSingleCommand("T,1,0,13");
            mySystem.version.compatibility.id = myDll.commandSingleAns[5];
            mySystem.version.compatibility.partNumber = myDll.commandSingleAns[8];
            mySystem.version.compatibility.version = myDll.commandSingleAns[6];
            mySystem.version.compatibility.release = myDll.commandSingleAns[7];

            //OSC
            myDll.CMSingleCommand("T,1,6,13,1");
            mySystem.version.oscBoot.id = myDll.commandSingleAns[5];
            mySystem.version.oscBoot.partNumber = myDll.commandSingleAns[8];
            mySystem.version.oscBoot.version = myDll.commandSingleAns[6];
            mySystem.version.oscBoot.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,6,13,2");
            mySystem.version.oscWce.id = myDll.commandSingleAns[5];
            mySystem.version.oscWce.partNumber = myDll.commandSingleAns[8];
            mySystem.version.oscWce.version = myDll.commandSingleAns[6];
            mySystem.version.oscWce.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,6,13,3");
            mySystem.version.oscxDll.id = myDll.commandSingleAns[5];
            mySystem.version.oscxDll.partNumber = myDll.commandSingleAns[8];
            mySystem.version.oscxDll.version = myDll.commandSingleAns[6];
            mySystem.version.oscxDll.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,6,13,4");
            mySystem.version.oscxApp.id = myDll.commandSingleAns[5];
            mySystem.version.oscxApp.partNumber = myDll.commandSingleAns[8];
            mySystem.version.oscxApp.version = myDll.commandSingleAns[6];
            mySystem.version.oscxApp.release = myDll.commandSingleAns[7];

            //FPGA
            myDll.CMSingleCommand("T,1,5,13");
            mySystem.version.fpga.id = myDll.commandSingleAns[5];
            mySystem.version.fpga.partNumber = myDll.commandSingleAns[8];
            mySystem.version.fpga.version = myDll.commandSingleAns[6];
            mySystem.version.fpga.release = myDll.commandSingleAns[7];

            //Controller
            myDll.CMSingleCommand("T,1,2,13");
            mySystem.version.Controller.id = myDll.commandSingleAns[5];
            mySystem.version.Controller.partNumber = myDll.commandSingleAns[8];
            mySystem.version.Controller.version = myDll.commandSingleAns[6];
            mySystem.version.Controller.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,2,13,1");
            mySystem.version.Controller2.id = myDll.commandSingleAns[5];
            mySystem.version.Controller2.partNumber = myDll.commandSingleAns[8];
            mySystem.version.Controller2.version = myDll.commandSingleAns[6];
            mySystem.version.Controller2.release = myDll.commandSingleAns[7];

            //Reader
            myDll.CMSingleCommand("T,1,3,13");
            mySystem.version.readerHost.id = myDll.commandSingleAns[5];
            mySystem.version.readerHost.partNumber = myDll.commandSingleAns[8];
            mySystem.version.readerHost.version = myDll.commandSingleAns[6];
            mySystem.version.readerHost.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,3,13,1");
            mySystem.version.readerDsp.id = myDll.commandSingleAns[5];
            mySystem.version.readerDsp.partNumber = myDll.commandSingleAns[8];
            mySystem.version.readerDsp.version = myDll.commandSingleAns[6];
            mySystem.version.readerDsp.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,3,13,2");
            mySystem.version.readerFpga.id = myDll.commandSingleAns[5];
            mySystem.version.readerFpga.partNumber = myDll.commandSingleAns[8];
            mySystem.version.readerFpga.version = myDll.commandSingleAns[6];
            mySystem.version.readerFpga.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,3,13,3");
            mySystem.version.readerTape.id = myDll.commandSingleAns[5];
            mySystem.version.readerTape.partNumber = myDll.commandSingleAns[8];
            mySystem.version.readerTape.version = myDll.commandSingleAns[6];
            mySystem.version.readerTape.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,3,13,4");
            mySystem.version.readerMagnetic.id = myDll.commandSingleAns[5];
            mySystem.version.readerMagnetic.partNumber = myDll.commandSingleAns[8];
            mySystem.version.readerMagnetic.version = myDll.commandSingleAns[6];
            mySystem.version.readerMagnetic.release = myDll.commandSingleAns[7];

            //Safe board
            myDll.CMSingleCommand("T,1,4,13");
            mySystem.version.safe.id = myDll.commandSingleAns[5];
            mySystem.version.safe.partNumber = myDll.commandSingleAns[8];
            mySystem.version.safe.version = myDll.commandSingleAns[6];
            mySystem.version.safe.release = myDll.commandSingleAns[7];

            //Cassettes
            myDll.CMSingleCommand("T,1,A,13");
            mySystem.version.cassetteA.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteA.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteA.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteA.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,B,13");
            mySystem.version.cassetteB.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteB.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteB.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteB.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,C,13");
            mySystem.version.cassetteC.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteC.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteC.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteC.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,D,13");
            mySystem.version.cassetteD.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteD.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteD.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteD.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,E,13");
            mySystem.version.cassetteE.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteE.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteE.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteE.release = myDll.commandSingleAns[7];
            myDll.CMSingleCommand("T,1,F,13");
            mySystem.version.cassetteF.id = myDll.commandSingleAns[5];
            mySystem.version.cassetteF.partNumber = myDll.commandSingleAns[8];
            mySystem.version.cassetteF.version = myDll.commandSingleAns[6];
            mySystem.version.cassetteF.release = myDll.commandSingleAns[7];
            if (mySystem.cassetteNumber>6)
            {
                myDll.CMSingleCommand("T,1,G,13");
                mySystem.version.cassetteG.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteG.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteG.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteG.release = myDll.commandSingleAns[7];
                myDll.CMSingleCommand("T,1,H,13");
                mySystem.version.cassetteH.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteH.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteH.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteH.release = myDll.commandSingleAns[7];
            }
            if (mySystem.cassetteNumber>8)
            {
                myDll.CMSingleCommand("T,1,I,13");
                mySystem.version.cassetteI.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteI.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteI.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteI.release = myDll.commandSingleAns[7];
                myDll.CMSingleCommand("T,1,J,13");
                mySystem.version.cassetteJ.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteJ.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteJ.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteJ.release = myDll.commandSingleAns[7];
            }
            if (mySystem.cassetteNumber>10)
            {
                myDll.CMSingleCommand("T,1,K,13");
                mySystem.version.cassetteK.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteK.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteK.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteK.release = myDll.commandSingleAns[7];
                myDll.CMSingleCommand("T,1,L,13");
                mySystem.version.cassetteL.id = myDll.commandSingleAns[5];
                mySystem.version.cassetteL.partNumber = myDll.commandSingleAns[8];
                mySystem.version.cassetteL.version = myDll.commandSingleAns[6];
                mySystem.version.cassetteL.release = myDll.commandSingleAns[7];
            }
            
            //Bag
            if (mySystem.family=="CM18B" | mySystem.family=="OM61")
            {
                myDll.CMSingleCommand("T,1,a,13");
                mySystem.version.bagContrA.id = myDll.commandSingleAns[5];
                mySystem.version.bagContrA.partNumber = myDll.commandSingleAns[8];
                mySystem.version.bagContrA.version = myDll.commandSingleAns[6];
                mySystem.version.bagContrA.release = myDll.commandSingleAns[7];
                myDll.CMSingleCommand("T,1,a,13,1");
                mySystem.version.bagSensorA.id = myDll.commandSingleAns[5];
                mySystem.version.bagSensorA.partNumber = myDll.commandSingleAns[8];
                mySystem.version.bagSensorA.version = myDll.commandSingleAns[6];
                mySystem.version.bagSensorA.release = myDll.commandSingleAns[7];
            }

            //Suite
            myDll.CMSingleCommand("T,1,0,84");
            mySystem.suiteCode = myDll.commandSingleAns[5];

            //CDF
            myDll.CMSingleCommand("T,1,3,21");
            mySystem.reader.cdfName = myDll.commandSingleAns[5];

            //Multicurrency
            myDll.CMSingleCommand("T,1,3,1");
            int i = (myDll.commandSingleAns.Length-5)/3;
            mySystem.reader.bankCfg = new Arca.SBankCfg[i];
            for (int x = 1;x<i+1;x++)
            {
                mySystem.reader.bankCfg[x-1].name= myDll.commandSingleAns[(x*3)+2];
                mySystem.reader.bankCfg[x-1].bankId = myDll.commandSingleAns[(x * 3) + 3];
                mySystem.reader.bankCfg[x-1].enabled = myDll.commandSingleAns[(x * 3) + 4];
            }

            myDll.CMSingleCommand("T,1,3,3");//currencymode 0=single 1-4=multi
            mySystem.reader.currencyMode = "SINGLE";
            if (myDll.commandSingleAns[5]=="1" | myDll.commandSingleAns[5]=="2" | myDll.commandSingleAns[5] == "3" | myDll.commandSingleAns[5] == "4")
            {
                mySystem.reader.currencyMode = "MULTI";
            }
            

            myDll.CMSingleCommand("T,1,3,5");
            mySystem.reader.currency = new Arca.SCurrency[16];
            for (i=1;i<=16;i++)
            {
                mySystem.reader.currency[i-1].name = myDll.commandSingleAns[(i * 3) + 2];
                mySystem.reader.currency[i - 1].version = myDll.commandSingleAns[(i * 3) + 3];
                mySystem.reader.currency[i - 1].bankId = myDll.commandSingleAns[(i * 3) + 4];
            }
            //version=6 enable=5 bankId=7

            myDll.CMSingleCommand("T,1,0,60,1"); //Photo Sensor Read
            mySystem.realtimePhoto.inCenter.value = Convert.ToInt32(myDll.commandSingleAns[6]);
            mySystem.realtimePhoto.inLeft.value = Convert.ToInt32(myDll.commandSingleAns[7]);
            mySystem.realtimePhoto.hMax.value = Convert.ToInt32(myDll.commandSingleAns[8]);
            mySystem.realtimePhoto.feed.value = Convert.ToInt32(myDll.commandSingleAns[9]);
            mySystem.realtimePhoto.c1.value = Convert.ToInt32(myDll.commandSingleAns[10]);
            mySystem.realtimePhoto.shift.value = Convert.ToInt32(myDll.commandSingleAns[11]);
            mySystem.realtimePhoto.inq.value = Convert.ToInt32(myDll.commandSingleAns[12]);
            mySystem.realtimePhoto.count.value = Convert.ToInt32(myDll.commandSingleAns[13]);
            mySystem.realtimePhoto.Out.value = Convert.ToInt32(myDll.commandSingleAns[14]);
            mySystem.realtimePhoto.c3.value = Convert.ToInt32(myDll.commandSingleAns[15]);
            mySystem.realtimePhoto.c4a.value = Convert.ToInt32(myDll.commandSingleAns[16]);
            mySystem.realtimePhoto.c4b.value = Convert.ToInt32(myDll.commandSingleAns[17]);
            mySystem.realtimePhoto.rej.value = Convert.ToInt32(myDll.commandSingleAns[18]);
            mySystem.realtimePhoto.inBox.value = Convert.ToInt32(myDll.commandSingleAns[19]);
            mySystem.realtimePhoto.cashLeft.value = Convert.ToInt32(myDll.commandSingleAns[26]);
            mySystem.realtimePhoto.cashRight.value = Convert.ToInt32(myDll.commandSingleAns[27]);
            if (mySystem.family!="OM61" & mySystem.family!="CM18B")
            {
                mySystem.realtimePhoto.thick.name = "ph_res7";
                mySystem.realtimePhoto.finrcyc.name = "ph_res8";
            }
            else
            {
                mySystem.realtimePhoto.thick.name = "ph_thick";
                mySystem.realtimePhoto.finrcyc.name = "ph_finrcyc";
            }
            mySystem.realtimePhoto.thick.value = Convert.ToInt32(myDll.commandSingleAns[28]);
            mySystem.realtimePhoto.finrcyc.value = Convert.ToInt32(myDll.commandSingleAns[29]);


            //CMConfig
            myDll.CMSingleCommand("T,1,0,15");

            string cmConfig =  Arca.Hex2Bin(myDll.commandSingleAns[5]);
            for (i = 15;i>=0;i--)
            {
                if(cmConfig.Substring(i,1)=="1")
                {
                    mySystem.cmConfig.bit[15-i].value = true;
                }
                else
                {
                    mySystem.cmConfig.bit[15 - i].value = false;
                }
            }

            //CM Option Config
            myDll.CMSingleCommand("T,1,0,28");
            string cmOptionConfig = Arca.Hex2Bin(myDll.commandSingleAns[5]);
            for (i = 15; i >= 0; i--)
            {
                if (cmOptionConfig.Substring(i, 1) == "1")
                {
                    mySystem.cmOption.bit[15 - i].value = true;
                }
                else
                {
                    mySystem.cmOption.bit[15 - i].value = false;
                }
            }

            //CM Option One Config
            myDll.CMSingleCommand("T,1,0,32");
            string cmOptionOneConfig = Arca.Hex2Bin(myDll.commandSingleAns[5]);
            for (i = 15; i >= 0; i--)
            {
                if (cmOptionOneConfig.Substring(i, 1) == "1")
                {
                    mySystem.cmOption1.bit[15 - i].value = true;
                }
                else
                {
                    mySystem.cmOption1.bit[15 - i].value = false;
                }
            }

            //CM Option Two Config
            myDll.CMSingleCommand("T,1,0,49");
            string cmOptionTwoConfig = Arca.Hex2Bin(myDll.commandSingleAns[5]);
            for (i = 31; i >= 0; i--)
            {
                if (cmOptionTwoConfig.Substring(i, 1) == "1")
                {
                    mySystem.cmOption2.bit[31 - i].value = true;
                }
                else
                {
                    mySystem.cmOption2.bit[31 - i].value = false;
                }
            }

            //Error log
            
            int lastErrorLog = Convert.ToInt32(mySystem.errorLog.Count() - 1);
            errorLogLine = new string[lastErrorLog + 1];
            errorLogLine[0] = string.Format("│   0 │    │           │{0,12}│  │  │     │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │   │", mySystem.errorLog[0].life);
            string time = "";
            Int32 life = 0;
            string titleCM18 = "│{0,4} │ {1,1}  │      {2,5}│{3,12}│{4,2}│{5,2}│{6,4} │{7,3}│{8,3}│{9,3}│{10,3}│{11,3}│{12,3}|{13,3}│{14,3}│{15,3}│{16,3}│{17,3}│{18,3}│{19,3}│";
            string titleCM18T = "│{0,4} │ {1,1}  │      {2,5}│{3,12}│{4,2}│{5,2}│{6,4} │{7,3}│{8,3}│{9,3}│{10,3}│{11,3}│{12,3}|{13,3}│{14,3}│{15,3}│{16,3}│{17,3}│{18,3}│{19,3}│{20,3}│{21,3}│{22,3}│{23,3}│{24,3}│{25,3}│{26,3}│";
            for (i = 1; i <= lastErrorLog; i++)
            {
                time = "";
                life = Convert.ToInt32(mySystem.errorLog[i].life);
                this.Text = "life=" + mySystem.errorLog[i].life.ToString();
                if (Convert.ToString(mySystem.errorLog[i].life).Length > 8)
                {
                    time = Convert.ToString(mySystem.errorLog[i].life).Substring(0, 2) + ":" + Convert.ToString(mySystem.errorLog[i].life).Substring(2, 2); 
                    life = Convert.ToInt32(Convert.ToString(mySystem.errorLog[i].life).Substring(4));
                }
                //if (mySystem.errorLog[i].replyCode != "581" && mySystem.errorLog[i].replyCode != "580" && mySystem.errorLog[i].replyCode != "401")
                //{
                try
                {
                    
                    
                    if (mySystem.family.ToUpper() == "CM18" || mySystem.family.ToUpper() == "CM18SOLO")
                    {
                        errorLogLine[i] = string.Format(titleCM18, mySystem.errorLog[i].recordNumber, "", time, life, mySystem.errorLog[i].opSide,
                        mySystem.errorLog[i].opId, mySystem.errorLog[i].replyCode, mySystem.errorLog[i].F1Status.Substring(1), mySystem.errorLog[i].F2Status.Substring(1), mySystem.errorLog[i].F3Status.Substring(1),
                        mySystem.errorLog[i].F4Status.Substring(1), mySystem.errorLog[i].FAStatus.Substring(1), mySystem.errorLog[i].FBStatus.Substring(1), mySystem.errorLog[i].FCStatus.Substring(1), mySystem.errorLog[i].FDStatus.Substring(1),
                        mySystem.errorLog[i].FEStatus.Substring(1), mySystem.errorLog[i].FFStatus.Substring(1), mySystem.errorLog[i].FGStatus.Substring(1), mySystem.errorLog[i].FHStatus.Substring(1), mySystem.errorLog[i].FIStatus.Substring(1));
                    }
                    else
                    {
                        errorLogLine[i] = string.Format(titleCM18T, mySystem.errorLog[i].recordNumber, "", time, life, mySystem.errorLog[i].opSide,
                        mySystem.errorLog[i].opId, mySystem.errorLog[i].replyCode, mySystem.errorLog[i].F1Status.Substring(1), mySystem.errorLog[i].F2Status.Substring(1), mySystem.errorLog[i].F3Status.Substring(1),
                        mySystem.errorLog[i].F4Status.Substring(1), mySystem.errorLog[i].FAStatus.Substring(1), mySystem.errorLog[i].FBStatus.Substring(1), mySystem.errorLog[i].FCStatus.Substring(1), mySystem.errorLog[i].FDStatus.Substring(1),
                        mySystem.errorLog[i].FEStatus.Substring(1), mySystem.errorLog[i].FFStatus.Substring(1), mySystem.errorLog[i].FGStatus.Substring(1), mySystem.errorLog[i].FHStatus.Substring(1), mySystem.errorLog[i].FIStatus.Substring(1),
                        mySystem.errorLog[i].FJStatus.Substring(1), mySystem.errorLog[i].FKStatus.Substring(1), mySystem.errorLog[i].FLStatus.Substring(1), mySystem.errorLog[i].FMStatus.Substring(1), mySystem.errorLog[i].FNStatus.Substring(1),
                        mySystem.errorLog[i].FOStatus.Substring(1), mySystem.errorLog[i].FPStatus.Substring(1));
                    }
                }
                catch (Exception ex)
                {
                    lblMain.Text = ex.Message + " - PRESS ANY KEY TO CONTINUE";
                    Arca.WaitingKey();
                }  
                //}
            }
        }

        string ErrorLogSave(int rowIndex, string suffix ="")
        {
            string result = "PASS";
            if ((mySystem.partNumber == "43664" || mySystem.partNumber == "43543" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543-TEMP") && suffix=="_a")
                return result;
            string errorlogFolder = cfg.GetValue("option", "errorlogFolder") + mySystem.family.Substring(0,4) + "\\";
            SystemInformationUpdate();
            Arca.WriteMessage(lblMain, 32, Color.White); //Salvataggio error log in corso
            string errorLog = "";
            string date = DateTime.Now.DayOfWeek.ToString().Substring(0,3) + "  " +  DateTime.Now.ToShortDateString();
            
            //┌ ─ ┬ ┐ ├ ┼ ┤ │ └ ┴ ┘ 
            errorLog += "┌────────┬─────────────────┬──extern label data──┬─ CM CQA rel 1.0.0 ────────┐" + (char)13 + (char)10;
            errorLog +=string.Format("│  {0, -4}  │ S.N.:  {1,8} │      {2,12}   │ Data :   {3,11}  │" + (char)13 + (char)10, mySystem.protInterface,mySystem.serialNumber.Substring(5), mySystem.serialNumber,date); //0=4 1=8 2=8 3=11
            errorLog += "├────────┴─────────────────┴─────────────┬───────┴───────────────────────────┤" + (char)13 + (char)10;
            errorLog +=string.Format( "|  Client  : {0,22}      │   Site : {1,21}    │" + (char)13 + (char)10,mySystem.clientName,mySystem.partNumber);
            errorLog += "└────────────────────────────────────────┴───────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10; 
            errorLog += "┌──────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                  SUITE NAME                  │" + (char)13 + (char)10;
            errorLog += "├──────────────────────────────────────────────┤" + (char)13 + (char)10;
            errorLog += string.Format("│ {0,-20}                         │" + (char)13 + (char)10, mySystem.suiteCode);
            errorLog += "└──────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "┌───────────────┬────────┬────────────────┬─────────┬─────────┬──────┬───────┐" + (char)13 + (char)10;
            errorLog += "│  Module  Name │   Id   │   Part. Num.   │ Release │ Version │ Den  │  Num  │" + (char)13 + (char)10;
            errorLog += "├───────────────┼────────┼────────────────┼─────────┼─────────┼──────┼───────┤" + (char)13 + (char)10;
            errorLog += string.Format("│ Compatibility │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.compatibility.id, mySystem.version.compatibility.partNumber, mySystem.version.compatibility.release, mySystem.version.compatibility.version);
            errorLog += string.Format("│ OSC Board     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.oscBoot.id, mySystem.version.oscBoot.partNumber, mySystem.version.oscBoot.release, mySystem.version.oscBoot.version);
            errorLog += string.Format("│ OSC Board     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.oscWce.id, mySystem.version.oscWce.partNumber, mySystem.version.oscWce.release, mySystem.version.oscWce.version);
            errorLog += string.Format("│ OSC Board     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.oscxDll.id, mySystem.version.oscxDll.partNumber, mySystem.version.oscxDll.release, mySystem.version.oscxDll.version);
            errorLog += string.Format("│ OSC Board     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.oscxApp.id, mySystem.version.oscxApp.partNumber, mySystem.version.oscxApp.release, mySystem.version.oscxApp.version);
            errorLog += string.Format("│ FPGA module   │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.fpga.id, mySystem.version.fpga.partNumber, mySystem.version.fpga.release, mySystem.version.fpga.version);
            errorLog += string.Format("│ Controller    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.Controller.id, mySystem.version.Controller.partNumber, mySystem.version.Controller.release, mySystem.version.Controller.version);
            errorLog += string.Format("│ Controller    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.Controller2.id, mySystem.version.Controller2.partNumber, mySystem.version.Controller2.release, mySystem.version.Controller2.version);
            errorLog += string.Format("│ Note read     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.readerHost.id, mySystem.version.readerHost.partNumber, mySystem.version.readerHost.release, mySystem.version.readerHost.version);
            errorLog += string.Format("│ Note read     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.readerDsp.id, mySystem.version.readerDsp.partNumber, mySystem.version.readerDsp.release, mySystem.version.readerDsp.version);
            errorLog += string.Format("│ Note read     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.readerFpga.id, mySystem.version.readerFpga.partNumber, mySystem.version.readerFpga.release, mySystem.version.readerFpga.version);
            errorLog += string.Format("│ Note read     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.readerTape.id, mySystem.version.readerTape.partNumber, mySystem.version.readerTape.release, mySystem.version.readerTape.version);
            errorLog += string.Format("│ Note read     │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.readerMagnetic.id, mySystem.version.compatibility.partNumber, mySystem.version.compatibility.release, mySystem.version.compatibility.version);
            errorLog += string.Format("│ Safe board    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │      │       │" + (char)13 + (char)10, mySystem.version.safe.id, mySystem.version.safe.partNumber, mySystem.version.safe.release, mySystem.version.safe.version);
            errorLog += string.Format("│ Cassette A    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteA.id, mySystem.version.cassetteA.partNumber, mySystem.version.cassetteA.release, mySystem.version.cassetteA.version,"","");
            errorLog += string.Format("│ Cassette B    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteB.id, mySystem.version.cassetteB.partNumber, mySystem.version.cassetteB.release, mySystem.version.cassetteB.version, "", "");
            errorLog += string.Format("│ Cassette C    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteC.id, mySystem.version.cassetteC.partNumber, mySystem.version.cassetteC.release, mySystem.version.cassetteC.version, "", "");
            errorLog += string.Format("│ Cassette D    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteD.id, mySystem.version.cassetteD.partNumber, mySystem.version.cassetteD.release, mySystem.version.cassetteD.version, "", "");
            errorLog += string.Format("│ Cassette E    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteE.id, mySystem.version.cassetteE.partNumber, mySystem.version.cassetteE.release, mySystem.version.cassetteE.version, "", "");
            errorLog += string.Format("│ Cassette F    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteF.id, mySystem.version.cassetteF.partNumber, mySystem.version.cassetteF.release, mySystem.version.cassetteF.version, "", "");
            if (mySystem.cassetteNumber > 6)
            {
                errorLog += string.Format("│ Cassette G    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteG.id, mySystem.version.cassetteG.partNumber, mySystem.version.cassetteG.release, mySystem.version.cassetteG.version, "", "");
                errorLog += string.Format("│ Cassette H    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteH.id, mySystem.version.cassetteH.partNumber, mySystem.version.cassetteH.release, mySystem.version.cassetteH.version, "", "");
            }
            if (mySystem.cassetteNumber > 8)
            {
                errorLog += string.Format("│ Cassette I    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteI.id, mySystem.version.cassetteI.partNumber, mySystem.version.cassetteI.release, mySystem.version.cassetteI.version, "", "");
                errorLog += string.Format("│ Cassette J    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteJ.id, mySystem.version.cassetteJ.partNumber, mySystem.version.cassetteJ.release, mySystem.version.cassetteJ.version, "", "");
            }
            if (mySystem.cassetteNumber > 10)
            {
                errorLog += string.Format("│ Cassette K    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteK.id, mySystem.version.cassetteK.partNumber, mySystem.version.cassetteK.release, mySystem.version.cassetteK.version, "", "");
                errorLog += string.Format("│ Cassette L    │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.cassetteL.id, mySystem.version.cassetteL.partNumber, mySystem.version.cassetteL.release, mySystem.version.cassetteL.version, "", "");
            }
            if (mySystem.family=="OM61" | mySystem.family=="CM18B")
            {
                errorLog += string.Format("│ DEPContr (a)  │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.bagContrA.id, mySystem.version.bagContrA.partNumber, mySystem.version.bagContrA.release, mySystem.version.bagContrA.version, "", "");
                errorLog += string.Format("│ DEPSensor(a)  │  {0,4}  │  {1,12}  │   {2,4}  │   {3,4}  │ {4,4} │  {5,4} │" + (char)13 + (char)10, mySystem.version.bagSensorA.id, mySystem.version.bagSensorA.partNumber, mySystem.version.bagSensorA.release, mySystem.version.bagSensorA.version, "", "");
            }
            errorLog += "└───────────────┴────────┴────────────────┴─────────┴─────────┴──────┴───────┘" + (char)13 + (char)10;
            errorLog += "┌────────────────────────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += string.Format("│ CDF  {0,-20}                                                  │" + (char)13 + (char)10, mySystem.reader.cdfName);
            errorLog += "└────────────────────────────────────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "┌─────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│    Multicurrency mode   │" + (char)13 + (char)10;
            errorLog += "├─────┬───────┬───────────┤" + (char)13 + (char)10;
            errorLog += "│ Name│  Enab │   BankId  │" + (char)13 + (char)10;
            errorLog += "├─────┼───────┼───────────┤" + (char)13 + (char)10;
            for (int i = 0;i< mySystem.reader.bankCfg.Length;i++)
            {
                errorLog += string.Format("│  {0,-2} │      {1}│          {2}│" + (char)13 + (char)10, mySystem.reader.bankCfg[i].name, mySystem.reader.bankCfg[i].enabled, mySystem.reader.bankCfg[i].bankId);
            }
            errorLog += "└─────┴───────┴───────────┘" + (char)13 + (char)10;
            errorLog += "┌─────────┬──────┬────────┐" + (char)13 + (char)10;
            errorLog += "│ Version │ Name │ BankId │" + (char)13 + (char)10;
            errorLog += "├─────────┼──────┼────────┤" + (char)13 + (char)10;
            for (int i = 0; i < 16; i++)
            {
                errorLog += string.Format("│     {0,-4}│    {1,2}│      {2,2}│" + (char)13 + (char)10, mySystem.reader.currency[i].version, mySystem.reader.currency[i].name, mySystem.reader.currency[i].bankId);
            }
            errorLog += "└─────────┴──────┴────────┘" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                        Photo sensor read                          │" + (char)13 + (char)10;
            errorLog += "├────────────────┬────────────────┬────────────────┬────────────────┤" + (char)13 + (char)10;
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.inCenter.name,mySystem.realtimePhoto.inCenter.value, mySystem.realtimePhoto.inLeft.name, mySystem.realtimePhoto.inLeft.value, mySystem.realtimePhoto.hMax.name, mySystem.realtimePhoto.hMax.value, mySystem.realtimePhoto.feed.name, mySystem.realtimePhoto.feed.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.c1.name, mySystem.realtimePhoto.c1.value, mySystem.realtimePhoto.shift.name, mySystem.realtimePhoto.shift.value, mySystem.realtimePhoto.inq.name, mySystem.realtimePhoto.inq.value, mySystem.realtimePhoto.count.name, mySystem.realtimePhoto.count.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.Out.name, mySystem.realtimePhoto.Out.value, mySystem.realtimePhoto.c3.name, mySystem.realtimePhoto.c3.value, mySystem.realtimePhoto.c4a.name, mySystem.realtimePhoto.c4a.value, mySystem.realtimePhoto.c4b.name, mySystem.realtimePhoto.c4b.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.rej.name, mySystem.realtimePhoto.rej.value, mySystem.realtimePhoto.res1.name, mySystem.realtimePhoto.res1.value, mySystem.realtimePhoto.res2.name, mySystem.realtimePhoto.res2.value, mySystem.realtimePhoto.res3.name, mySystem.realtimePhoto.res3.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.res4.name, mySystem.realtimePhoto.res4.value, mySystem.realtimePhoto.res5.name, mySystem.realtimePhoto.res5.value, mySystem.realtimePhoto.res6.name, mySystem.realtimePhoto.res6.value, mySystem.realtimePhoto.res7.name, mySystem.realtimePhoto.res7.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.cashLeft.name, mySystem.realtimePhoto.cashLeft.value, mySystem.realtimePhoto.cashRight.name, mySystem.realtimePhoto.cashRight.value, mySystem.realtimePhoto.thick.name, mySystem.realtimePhoto.thick.value, mySystem.realtimePhoto.finrcyc.name, mySystem.realtimePhoto.finrcyc.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.res10.name, mySystem.realtimePhoto.res10.value, mySystem.realtimePhoto.res11.name, mySystem.realtimePhoto.res11.value, mySystem.realtimePhoto.res12.name, mySystem.realtimePhoto.res12.value, mySystem.realtimePhoto.res13.name, mySystem.realtimePhoto.res13.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.res14.name, mySystem.realtimePhoto.res14.value, mySystem.realtimePhoto.res15.name, mySystem.realtimePhoto.res15.value, mySystem.realtimePhoto.res16.name, mySystem.realtimePhoto.res16.value, mySystem.realtimePhoto.res17.name, mySystem.realtimePhoto.res17.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.res18.name, mySystem.realtimePhoto.res18.value, mySystem.realtimePhoto.res19.name, mySystem.realtimePhoto.res19.value, mySystem.realtimePhoto.res20.name, mySystem.realtimePhoto.res20.value, mySystem.realtimePhoto.res21.name, mySystem.realtimePhoto.res21.value);
            errorLog += string.Format("│{0,-12}{1,3} │{2,-12}{3,3} │{4,-12}{5,3} │{6,-12}{7,3} │" + (char)13 + (char)10, mySystem.realtimePhoto.res22.name, mySystem.realtimePhoto.res22.value, mySystem.realtimePhoto.res23.name, mySystem.realtimePhoto.res23.value, mySystem.realtimePhoto.res24.name, mySystem.realtimePhoto.res24.value, mySystem.realtimePhoto.res25.name, mySystem.realtimePhoto.res25.value);
            errorLog += "└────────────────┴────────────────┴────────────────┴────────────────┘" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                       CM Config                           │" + (char)13 + (char)10;
            errorLog += "├───────────────────────────────────────────────────────────┤" + (char)13 + (char)10;
            string value = "N";
            for (int i = 0;i<=15;i++)
            {
                if (mySystem.cmConfig.bit[i].description != "reserved")
                {
                    value = "N";
                    if (mySystem.cmConfig.bit[i].value == true)
                    {
                        value = "Y";
                    }
                    errorLog += string.Format("│({0,6}) {1,-45} ({2,1}) │" + (char)13 + (char)10, mySystem.cmConfig.bit[i].address, mySystem.cmConfig.bit[i].description, value);
                }
            }
            
            errorLog += "└───────────────────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                      CM Option Config                     │" + (char)13 + (char)10;
            errorLog += "├───────────────────────────────────────────────────────────┤" + (char)13 + (char)10;
            for (int i = 0; i <= 15; i++)
            {
                if (mySystem.cmOption.bit[i].description != "reserved")
                {
                    value = "N";
                    if (mySystem.cmOption.bit[i].value == true)
                    {
                        value = "Y";
                    }
                    errorLog += string.Format("│({0,6}) {1,-45} ({2,1}) │" + (char)13 + (char)10, mySystem.cmOption.bit[i].address, mySystem.cmOption.bit[i].description, value);
                }
            }
            errorLog += "└───────────────────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                   CM Option ONE Config                    │" + (char)13 + (char)10;
            errorLog += "├───────────────────────────────────────────────────────────┤" + (char)13 + (char)10;
            for (int i = 0; i <= 15; i++)
            {
                if (mySystem.cmOption1.bit[i].description != "reserved")
                {
                    value = "N";
                    if (mySystem.cmOption1.bit[i].value == true)
                    {
                        value = "Y";
                    }
                    errorLog += string.Format("│({0,6}) {1,-45} ({2,1}) │" + (char)13 + (char)10, mySystem.cmOption1.bit[i].address, mySystem.cmOption1.bit[i].description, value);
                }
            }
            errorLog += "└───────────────────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│                   CM Option TWO Config                    │" + (char)13 + (char)10;
            errorLog += "├───────────────────────────────────────────────────────────┤" + (char)13 + (char)10;
            for (int i = 0; i <= 31; i++)
            {
                if (mySystem.cmOption2.bit[i].description != "reserved")
                {
                    value = "N";
                    if (mySystem.cmOption2.bit[i].value == true)
                    {
                        value = "Y";
                    }
                    errorLog += string.Format("│({0,10}) {1,-41} ({2,1}) │" + (char)13 + (char)10, mySystem.cmOption2.bit[i].address, mySystem.cmOption2.bit[i].description, value);
                }
            }
            errorLog += "└───────────────────────────────────────────────────────────┘" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "┌───────────────────────────────────────────────┬───────────────────────────────────────────────────────────────────────────────┐" + (char)13 + (char)10;
            errorLog += "│          H I G H     L E V E L                │                                      M O D U L E S                            │" + (char)13 + (char)10;
            errorLog += "│          I N F O R M A T I O N                │                                      S T A T U S                              │" + (char)13 + (char)10;
            errorLog += "├──────────┬───────────┬────────────┬─────┬─────┼───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┤" + (char)13 + (char)10;
            errorLog += "│  record  │time       │   Note     │Oper.│ Ret │fed│Cnt│Idn│Saf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│Inf│" + (char)13 + (char)10;
            errorLog += "│ Num │type│           │   Number   │S. Id│ Code│sta│sta│sta│sta│ A │ B │ C │ D │ E │ F │ G │ H │ I │ J │ K │ L │ M │ N │ O │ P │" + (char)13 + (char)10;
            errorLog += "├─────┼────┼───────────┼────────────┼──┬──┼─────┼───┼───┼───┼───┼───┼───┼───┼───┼───┼───┼───┼───┼───┼───┤───┤───┤───┤───┤───┤───┤" + (char)13 + (char)10;
            for (int i = 0; i<errorLogLine.Length;i++)
            {
                errorLog += errorLogLine[i] + "" + (char)13 + (char)10;
            }
            errorLog += "└─────┴────┴───────────┴────────────┴──┴──┴─────┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┴───┘" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            errorLog += "" + (char)13 + (char)10;
            int totalLife = mySystem.errorLog[0].life - mySystem.errorLog[1].life;
            errorLog += "┌──────────────────────────────────────────────┬──────────────────────────┐" + (char)13 + (char)10;
            errorLog += string.Format("│            CM27 ERROR LOG STATISTIC          │   Note Number {0,10} │" + (char)13 + (char)10, totalLife);
            errorLog += "├──────────────────────────────────────────────┼───────┬───────┬──────────┤" + (char)13 + (char)10;
            errorLog += "│    error code and description                │ quant │ Perc  │rate 1/bnk│" + (char)13 + (char)10;
            errorLog += "├──────────────────────────────────────────────┼───────┼───────┼──────────┤" + (char)13 + (char)10;
            if (jamList.totalErrors>0)
            {
                errorLog += string.Format("│ TOTAL ERRORS DETECTED                        │ {0,5} │ ----  │{1,9} │" + (char)13 + (char)10, jamList.totalErrors, totalLife / jamList.totalErrors);
            }
            else
            {
                errorLog += "│ TOTAL ERRORS DETECTED                        │     0 │ ----  │        - │" + (char)13 + (char)10;
            }
            
            errorLog += "├──────────────────────────────────────────────┼───────┼───────┼──────────┤" + (char)13 + (char)10;

            if (jamList.feedErrors>0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Feeder", jamList.feedErrors, (((double)100 / (double)jamList.totalErrors) * jamList.feedErrors).ToString("0.##"),totalLife/jamList.feedErrors );
            }
            if (jamList.controllerErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Upper track", jamList.controllerErrors, (((double)100 / (double)jamList.totalErrors) * jamList.controllerErrors).ToString("0.##"), totalLife / jamList.controllerErrors);
            }
            if (jamList.readerErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Reader", jamList.readerErrors, (((double)100 / (double)jamList.totalErrors) * jamList.readerErrors).ToString("0.##"), totalLife / jamList.readerErrors);
            }
            if (jamList.safeErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Safe", jamList.safeErrors, (((double)100 / (double)jamList.totalErrors) * jamList.safeErrors).ToString("0.##"), totalLife / jamList.safeErrors);
            }
            if (jamList.cassetteAErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette A", jamList.cassetteAErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteAErrors).ToString("0.##"), totalLife / jamList.cassetteAErrors);
            }
            if (jamList.cassetteBErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette B", jamList.cassetteBErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteBErrors).ToString("0.##"), totalLife / jamList.cassetteBErrors);
            }
            if (jamList.cassetteCErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette C", jamList.cassetteCErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteCErrors).ToString("0.##"), totalLife / jamList.cassetteCErrors);
            }
            if (jamList.cassetteDErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette D", jamList.cassetteDErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteDErrors).ToString("0.##"), totalLife / jamList.cassetteDErrors);
            }
            if (jamList.cassetteEErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette E", jamList.cassetteEErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteEErrors).ToString("0.##"), totalLife / jamList.cassetteEErrors);
            }
            if (jamList.cassetteFErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette F", jamList.cassetteFErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteFErrors).ToString("0.##"), totalLife / jamList.cassetteFErrors);
            }
            if (jamList.cassetteGErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette G", jamList.cassetteGErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteGErrors).ToString("0.##"), totalLife / jamList.cassetteGErrors);
            }
            if (jamList.cassetteHErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette H", jamList.cassetteHErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteHErrors).ToString("0.##"), totalLife / jamList.cassetteHErrors);
            }
            if (jamList.cassetteIErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette I", jamList.cassetteIErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteIErrors).ToString("0.##"), totalLife / jamList.cassetteIErrors);
            }
            if (jamList.cassetteJErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette J", jamList.cassetteJErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteJErrors).ToString("0.##"), totalLife / jamList.cassetteJErrors);
            }
            if (jamList.cassetteKErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette K", jamList.cassetteKErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteKErrors).ToString("0.##"), totalLife / jamList.cassetteKErrors);
            }
            if (jamList.cassetteLErrors > 0)
            {
                errorLog += string.Format("│ {0,-20}                         │  {1,4} │{2,6}%│{3,9} │" + (char)13 + (char)10, "Cassette L", jamList.cassetteLErrors, (((double)100 / (double)jamList.totalErrors) * jamList.cassetteLErrors).ToString("0.##"), totalLife / jamList.cassetteLErrors);
            }

            errorLog += "├──────────────────────────────────────────────┼───────┼───────┼──────────┤" + (char)13 + (char)10;
            errorLog += "│ DUPLICATED ERRORS (code P P )                │       │       │          │" + (char)13 + (char)10;
            errorLog += "└──────────────────────────────────────────────┴───────┴───────┴──────────┘" + (char)13 + (char)10;
            
            try
            {
                while (File.Exists(errorlogFolder + mySystem.serialNumber + suffix + ".txt"))
                    suffix = "_" + suffix;
                StreamWriter serverErrorLogFile = new StreamWriter(errorlogFolder + mySystem.serialNumber + suffix + ".txt");
                serverErrorLogFile.WriteLine(errorLog);
                serverErrorLogFile.Close();
                if (suffix != "") { Arca.LogWrite(logFileName, ",034" + errorlogFolder + mySystem.serialNumber + suffix + ".txt"); }
                
            }
            catch (Exception ex)
            {
                while (File.Exists(errorlogFolder + mySystem.serialNumber + suffix + ".txt"))
                    suffix = "_" + suffix;
                StreamWriter localErrorLogFile = new StreamWriter(mySystem.serialNumber + suffix + ".txt");
                localErrorLogFile.WriteLine(errorLog);
                localErrorLogFile.Close();
                if (suffix != "") { Arca.LogWrite(logFileName, ",034" + mySystem.serialNumber + suffix + ".txt"); }
            }
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            return result;
        }

        string ErrorLogInit(int rowIndex)
        {
            Arca.WriteMessage(lblMain, 18, Color.White);
            string result = "PASS";
            result = myDll.CMSingleCommand("H,1,IIII", 2, 30000);
            if (result == "101" | result == "1")
            {
                result = "PASS";
            }
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            Arca.LogWrite(logFileName, ",308" + result);
            return result;
        }

        string Deposit(int rowIndex, string side = "R")
        {
            string result = "PASS";
            if (mySystem.partNumber == "43664" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543" || mySystem.partNumber == "43543-TEMP")
                return "PASS";
            string tasto = "";
            Arca.WriteMessage(lblMain, 9, Color.White); //Inserire una mazzetta di banconote e premere un tasto per avviare il deposito
            Arca.WaitingKey();
            if (mySystem.family.ToUpper() == "OM61" || mySystem.family.ToUpper() == "CM18SOLO" || mySystem.family.ToUpper() == "CM18SOLOT")
                side = "L";
        depositCommand:
            result = myDll.CMOpen(side);
            Arca.WriteMessage(lblMain, 24, Color.White); //Deposito in corso
            result = myDll.CMSingleCommand("D,1," + side + ",0,0000", 3, 60000);
            switch (result)
            {
                case "101":
                case "1":
                    result = "PASS";
                    break;
                case "204": //note on rigth output
                case "9": //note on rigth output
                case "205": //note on left output
                case "10": //note on left output
                case "206": //note on front output
                case "11": //note on front output
                    Arca.WriteMessage(lblMain, 47, Color.White); //Rimuovere le banconote dalle bocchette di uscita.
                    Arca.WaitingTime(1000);
                    goto depositCommand;
                case "207": //no input note
                case "12": //no input note
                    Arca.WriteMessage(lblMain, 46, Color.White); //Inserire le banconote nella bocchetta d'ingresso per avviare il deposito.
                    Arca.WaitingTime(1000);
                    goto depositCommand;
                default:
                    Arca.WriteMessage(lblMain, 205, Arca.errorColor,result); //Macchina in stato: {0}. Risolvere il problema e premere un tasto per ripetere o ESC per uscire
                    tasto = Arca.WaitingKey();
                    if (tasto=="ESC")
                        break;
                    goto depositCommand;
            }
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            Arca.LogWrite(logFileName, ",309" + result);
            myDll.CMClose(side);
            return result;
        }

        string EvoButtonTest(int rowIndex)
        {
            //T,1,6,60,7
            //se 201 abilita 0x4000, chiedi se i pulsanti hanno funzionato e disabilita 0x4000
            string result = "FAIL";
            string tasto = "";
            testStart:
            //leggi unit configuration
            Arca.WriteMessage(lblMain, 29);//Verifica attivazione bit 0x00000040 - EVO led handling in corso
            result = myDll.CMSingleCommand("T,1,0,49", 4);
            if (result!="101" && result!="1")
            {
                Arca.WriteMessage(lblMain, 215, Color.White, result);//Verifica abilitazione bit EVO Hangling fallita! Risposta {0}. Premere un tasto per ripetere, ESC per uscire
                result = "FAIL";
                tasto = Arca.WaitingKey();
                if (tasto == "ESC")
                    goto end;
                goto testStart;
            }
            if (Arca.Hex2Bin(myDll.commandSingleAns[5]).Substring(27, 1) == "1")
                return "PASS";
            if (Arca.Hex2Bin(myDll.commandSingleAns[5]).Substring(25,1)=="0") //Verifica bit EVO handlig attivo
            {
                //unit configuration errata. U per correggere, esc per uscire
                Arca.WriteMessage(lblMain, 216);//ATTENZIONE!! Il bit EVO LED Handling non è attivo, premere un tasto per attivarlo, ESC per uscire
                tasto = Arca.WaitingKey();
                if (tasto == "ESC")
                    goto end;
                bitEnabling:
                result = myDll.CMSingleCommand("F,1,23," + Arca.Uint2Hex(Arca.Hex2Uint(myDll.commandSingleAns[5]) + 0x40,8), 3);
                if (result != "101" && result != "1")
                {
                    Arca.WriteMessage(lblMain, 217, Color.White, result);//ATTENZIONE!! abilitazione bit EVO Hangling fallita! Risposta {0}. Premere un tasto per ripetere, ESC per uscire
                    result = "FAIL";
                    tasto = Arca.WaitingKey();
                    if (tasto == "ESC")
                        goto end;
                    goto bitEnabling;
                }
            }
        //esegui il test T,1,6,60,7
        buttonTest:
            Arca.WriteMessage(lblMain, 28, Color.White); //Premere un tasto per avviare il test dei pulsanti EVO
            Arca.WaitingKey();
            Arca.WriteMessage(lblMain, 44, Color.White); //Seguire le istruzione sul display della macchina
            result = myDll.CMSingleCommand("T,1,6,60,7", 4, 20000);
            switch (result)
            {
                case "101":
                case "1":
                    break;
                case "201":
                case "3":
                    //abilita 0x4000
                    Arca.WriteMessage(lblMain, 62);//L'attuale FW non permette il test guidato dei pulsanti. Premere un tasto per continuare
                    Arca.WaitingKey();
                    Arca.WriteMessage(lblMain, 63);//Modifica unit configuration per eseguire il test manuale dei pulsanti
                    result = myDll.CMSingleCommand("T,1,0,32", 4);
                    string cmOptionOne = myDll.commandSingleAns[5];
                    result = myDll.CMSingleCommand("F,1,9," + Arca.Uint2Hex(Arca.Hex2Uint(cmOptionOne) + 0x4000, 4), 4);
                    //esegui il test manualmente
                    Arca.WriteMessage(lblMain, 64);//Provare manualmete i pulsanti. Ogni pressione del pulsante, viene alternato il lampeggio e la luce fissa. Premere un tasto per continuare, ESC per uscire
                    tasto = Arca.WaitingKey();
                    //disabilita 0x4000
                    Arca.WriteMessage(lblMain, 65); //Ripristino unit configuration in corso
                    result = myDll.CMSingleCommand("F,1,9," + cmOptionOne.PadLeft(4, '0')) ;
                    if (tasto == "ESC")
                        goto end;
                    break;
                case "932":
                    //pressione del tasto non ricevuta
                    Arca.WriteMessage(lblMain, 214);//Nessuna pressione del tasto rilevata. Premere un tasto per ripetere, ESC per uscire
                    tasto = Arca.WaitingKey();
                    if (tasto == "ESC")
                        goto end;
                    goto buttonTest;
                default:
                    //errore generico
                    Arca.WriteMessage(lblMain, 213,Color.White,result);//Test fallito! Risposta {0}. Premere un tasto per ripetere, ESC per uscire
                    tasto = Arca.WaitingKey();
                    if (tasto == "ESC")
                        goto end;
                    goto buttonTest;
            }

            dgTest.Rows[rowIndex].Cells["colValore"].Value = "OK";
            result = "PASS";
            
            end:
            Arca.LogWrite(logFileName, ",307" + result);
            return result;
        }

        string TouchVerify(int rowIndex)
        {
            string result = "FAIL";
            //Arca.WriteMessage(lblMain, 26, Color.White); //Premere un tasto per avviare la verifica del touch
            //Arca.WaitingKey();
            //Arca.WriteMessage(lblMain, 44, Color.White); //Seguire le istruzione sul display della macchina

            //result = myDll.CMSingleCommand("T,1,6,18,2,Inserisci '2' e premi OK", 4, 60000);
            //if ((result != "1" && result != "101") || myDll.commandSingleAns[5] != "2")
            //{
            //    dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            //    dgTest.Rows[rowIndex].DefaultCellStyle.BackColor = Color.OrangeRed;
            //    dgTest.Rows[rowIndex].Cells[3].Value = "FAILED";
            //    goto end;
            //}
            Arca.WriteMessage(lblMain, 27); //Verificare che la versione del FW sia correttamente indicata sul display. Premere un tasto per continuare, ESC per uscire
            if (Arca.WaitingKey() == "ESC")
            {
                dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
                dgTest.Rows[rowIndex].DefaultCellStyle.BackColor = Color.OrangeRed;
                dgTest.Rows[rowIndex].Cells[3].Value = "FAILED";
                goto end;
            }

            dgTest.Rows[rowIndex].Cells["colValore"].Value = "OK";
            result = "PASS";
        end:
            Arca.LogWrite(logFileName, ",30F" + result);
            return result;
        }

        string GetCashDataTest(int rowIndex, string side = "R")
        {
            string result = "FAIL";
            Arca.WriteMessage(lblMain, 49, Color.White); //Get CASH DATA in corso
            result = Open(side);
            result = myDll.CMSingleCommand("G,1," + side, 3, 5000);
            if (result=="1" || result=="101")
            {
                mySystem.cashData.bnInCash = 0;
                for (int i = 0; i < mySystem.cassetteInstalled; i++)
                {
                    mySystem.cassettes[i].noteId = myDll.commandSingleAns[i*5 + 4];
                    mySystem.cassettes[i].noteNumber =Convert.ToInt32(myDll.commandSingleAns[i*5 + 5]);
                    mySystem.cassettes[i].freeCapacity = Convert.ToInt32(myDll.commandSingleAns[i*5 + 6]);
                    mySystem.cassettes[i].name = myDll.commandSingleAns[i*5 + 7];
                    mySystem.cassettes[i].enabled = myDll.commandSingleAns[i*5 + 8];
                    mySystem.cashData.bnInCash += mySystem.cassettes[i].noteNumber;
                }
                if (mySystem.cashData.bnInCash==0)
                {
                    result = "PASS";
                }
            }
            Arca.LogWrite(logFileName, ",407" + mySystem.cashData.bnInCash);
            dgTest.Rows[rowIndex].Cells["colValore"].Value = mySystem.cashData.bnInCash;
            result = CMClose(side);
            return result;
        }

        string Withdrawal(int rowIndex, string side = "R")
        {
            string tasto = "";
            string result = "PASS";
            if (mySystem.partNumber == "43664" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543" || mySystem.partNumber == "43543-TEMP")
                return "PASS";
            start:
            if (mySystem.family.ToUpper() == "OM61" || mySystem.family.ToUpper() == "CM18SOLO" || mySystem.family.ToUpper() == "CM18SOLOT")
                side = "L";
            myDll.CMOpen(side);
            string comando = "W,1," + side;
            GetCashDataTest(rowIndex, side);
            Arca.WriteMessage(lblMain, 48, Color.White); //Prelievo in corso
            if (mySystem.cashData.bnInCash==0)
            {
                result = "PASS";
                return result;
            }

            //if (mySystem.cashData.bnInCash <= 200)
            //{
            for (int i = 0 + mySystem.cd80Number; i<mySystem.cassetteInstalled;i++)
            {
                string cassName = mySystem.cassettes[i].name + mySystem.cassettes[i].name + mySystem.cassettes[i].name + mySystem.cassettes[i].name;
                comando += "," + cassName + "," + mySystem.cassettes[i].noteNumber;
            }
            //}

            //if (mySystem.cashData.bnInCash > 200)
            //{

            //    for (int i = 0 + mySystem.cd80Number; i < mySystem.cassetteInstalled; i++)
            //    {
            //        string cassName = mySystem.cassettes[i].name + mySystem.cassettes[i].name + mySystem.cassettes[i].name + mySystem.cassettes[i].name;
            //        int qty = (mySystem.cassettes[i].noteNumber / mySystem.cashData.bnInCash) * 200;
            //    }
            //}

            withdrawal:
            myDll.CMOpen(side);
            result = myDll.CMSingleCommand(comando, 3, 112000);
            switch (result)
            {
                case "101":
                case "1":
                case "210": //denomination empty
                case "14": //denomination empty
                    result = "PASS";
                    break;
                case "204": //note on rigth output
                case "9": //note on rigth output
                case "205": //note on left output
                case "10": //note on left output
                case "206": //note on front output
                case "11": //note on front output
                    Arca.WriteMessage(lblMain, 47, Color.White); //Rimuovere le banconote dalle bocchette di uscita.
                    Arca.WaitingTime(1000);
                    goto withdrawal;
                case "207": //no input note
                case "12": //no input note
                    Arca.WriteMessage(lblMain, 46, Color.White); //Inserire le banconote nella bocchetta d'ingresso per avviare il deposito.
                    Arca.WaitingTime(1000);
                    goto withdrawal;
                case "213":
                case "67":
                    if (side == "L")
                        side = "R";
                    else
                        side = "L";
                    break;
                default:
                    Arca.WriteMessage(lblMain, 205, Arca.errorColor, result); //Macchina in stato: {0}. Risolvere il problema e premere un tasto per ripetere o ESC per uscire
                    tasto = Arca.WaitingKey();
                    if (tasto == "ESC")
                    {
                        break;
                    }
                    goto withdrawal;
            }
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            Arca.LogWrite(logFileName, ",30A" + result);
            CMClose(side);
            return result;
        }

        string SkewControl(int rowIndex)
        {
            string result = "PASS";
            if (mySystem.partNumber == "43664" || mySystem.partNumber == "43664-TEMP" || mySystem.partNumber == "43543" || mySystem.partNumber == "43543-TEMP")
                return "PASS";
            string tasto = "";
            Arca.WriteMessage(lblMain, 50, Color.White); //Verificare lo skew in ogni cassetto. Premere un tasto per confermare la verifica, ESC per uscire
            
            tasto=Arca.WaitingKey();
            if (tasto=="ESC")
            {
                result = "KO";
            }
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            Arca.LogWrite(logFileName, ",30B" + result);
            return result;
        }

        void TestResult (int rowIndex, string result)
        {
            if (result == "PASS" | result=="OK")
            {
                dgTest.Rows[rowIndex].Cells[3].Value = "PASS";
                dgTest.Rows[rowIndex].DefaultCellStyle.BackColor = Color.LawnGreen;
            }
            else
            {
                dgTest.Rows[rowIndex].Cells[3].Value = "FAILED";
                dgTest.Rows[rowIndex].DefaultCellStyle.BackColor = Color.OrangeRed;
            }
            dgTest.Refresh();
        }

        public struct STest
        {
            public string function;
            public string name;
            public string[] product;
            public string awaited;
            public int line;
            public string parameter;
        }


        string _myReply;
        public event EventHandler ReplyChanged;

        public string MyReply
        {
            get
            {
                return _myReply;
            }
            set
            {
                _myReply = value;
                OnReplyChanged(ReplyChanged);
            }
        }
        void OnReplyChanged(object sender)
        {
            switch (MyReply.ToUpper())
            {
                case "OK":
                    break;
                default:
                    break;
            }
        }

        string SearchAbagTest (string serialNumber)
        {
            string result = "FAIL";
            string[] abagDbFolder = cfg.GetAllValues("option","abagDbFolder");
            string abagDbName = cfg.GetValue("option", "abagDbName");
            for (int i = 0; i < abagDbFolder.Length; i++)
            {
                File.Copy(Path.Combine(abagDbFolder[i], abagDbName), Path.Combine(Application.StartupPath, abagDbName), true);
                string abagConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    "Data Source=" + Path.Combine(Application.StartupPath, abagDbName) +
                    "; Persist Security Info=true;" + "Jet OLEDB:Database Password=;";
                OleDbConnection abagConnection = new OleDbConnection();
                OleDbCommand abagCommand = new OleDbCommand();
                OleDbDataReader abagDbReader;
                abagConnection.ConnectionString = abagConnectionString;
                abagConnection.Open();
                string abagSQL = "SELECT * FROM [archivio prove] WHERE num_macchina = '" + serialNumber + "' AND esito_test = -1";
                abagCommand = abagConnection.CreateCommand();
                abagCommand.CommandText = abagSQL;
                abagDbReader = abagCommand.ExecuteReader();

                while (abagDbReader.Read())
                {
                    result = "PASS";
                }

                abagConnection.Close();
                if (result == "PASS")
                    goto fine;
            }
            
            fine:
            return result;
        }

        string CQA()
        {
            xmlLog = new XMLFile("logCQA.xml", TestType.CQA);
            MyReply = xmlLog.AddProduct(mySystem.serialNumber, "result", "false", "SWver", Application.ProductVersion, "station", Environment.MachineName, "dateStart", DateTime.Now.Date.ToShortDateString(), "timeStart", DateTime.Now.ToLongTimeString());
            string result="FAIL";
            // serial number, data start, time start, computer name, sw version
            string logLine = "";
            logLine += "004" + mySystem.serialNumber; //Serial Number
            logLine += ",005" + Application.ProductVersion; //SW Version
            logLine += ",006" + System.Environment.MachineName; //Computer Name
            logLine += ",300" + DateTime.Now.Date.ToShortDateString(); //Data Start
            logLine += ",301" + DateTime.Now.ToLongTimeString(); //Time Start
            Arca.LogWrite(logFileName, logLine);

            IniFile iniTestList = new IniFile(Application.StartupPath + "\\testlist.ini");
            int testQty = 0;
            int testIndex = 1;
            int indexCqaTest = 0;
            STest tempTest = new STest();
            List<STest> testList = new List<STest>();

            tempTest.name = "-";
            while (tempTest.name!="")
            {
                tempTest.name = iniTestList.GetValue("test" + testIndex, "name");
                tempTest.product = iniTestList.GetValue("test" + testIndex, "product").ToString().Split(',');
                tempTest.function = iniTestList.GetValue("test" + testIndex, "function");
                tempTest.awaited = iniTestList.GetValue("test" + testIndex, "awaited");
                tempTest.parameter = iniTestList.GetValue("test" + testIndex, "parameter");
                for (int i = 0; i < tempTest.product.Count(); i++)
                {
                    if (tempTest.product[i].ToString() == mySystem.family || (mySystem.cd80TrayNumber > 0 && tempTest.product[i].ToString() == "CD80" ))
                    {
                        tempTest.line = i;
                        testList.Add(tempTest);
                        dgTest.Rows.Add(tempTest.name);
                        dgTest.Rows[testQty].DefaultCellStyle.BackColor = Color.LightYellow;
                        dgTest.Rows[testQty].Cells["colVerifica"].Value = tempTest.name;
                        dgTest.Rows[testQty].Cells["colAtteso"].Value = tempTest.awaited;
                        dgTest.Rows[testQty].Cells["colValore"].Value = "NOT TESTED";
                        testQty += 1;
                        indexCqaTest += 1;
                        break;
                    }
                }
                testIndex += 1;
            }
            Arca.WriteMessage(lblMain, 71);//Connessione in corso...
            if (mySystem.family.ToUpper() == "OM61" || mySystem.family.ToUpper() == "CM18SOLO" || mySystem.family.ToUpper() == "CM18SOLOT")
                CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB;
            int dll = myDll.CMConnect(ref CmConnParam);
            //if (dll == 0)
            //    goto portConnectionOk;
            //CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_SERIAL;
            //if ((dll = myDll.CMConnect(ref CmConnParam)) == 0)
            //    goto connectionOk;
            //CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB;
            //if ((dll = myDll.CMConnect(ref CmConnParam)) == 0)
            //    goto connectionOk;
            //CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_SERIAL_EASY;
            //if ((dll = myDll.CMConnect(ref CmConnParam)) == 0)
            //    goto connectionOk;
            //CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB_EASY;
            //if ((dll = myDll.CMConnect(ref CmConnParam)) == 0)
            //    goto connectionOk;
            //Arca.WriteMessage(lblMain, "Connection ERROR", Arca.errorColor);
            //return "CONNECTIONFAILED";

            //portConnectionOk:


            //Ricerca matricola nel database ABAG
            
            if (SearchAbagTest(mySystem.serialNumber) != "PASS")
            {
                Arca.WriteMessage(lblMain,"La macchina non ha superato il test ABAG. Premi un tasto per uscire",Arca.errorColor);
                Arca.WaitingKey();
                return result;
            }

            connection:
            result =myDll.CMSingleCommand("T,1,0,88",4);
            if (result.Length != 3 && result.Length != 1)
            {
                int i = 1;
                while (result.Length != 3 && result.Length != 1)
                {
                    if (i > 4)
                    {
                        Arca.WriteMessage(lblMain, 219, Arca.errorColor); //Connessione fallita, premere un tasto per ripetere, ESC per uscire
                        if (Arca.WaitingKey() == "ESC")
                            return "CONNECTIONFAILED";
                        goto connection;
                    }
                    switch (i)
                    {
                        case 1: //RS232
                            Arca.WriteMessage(lblMain, 72, Color.White, "RS232"); //Tentativo di connessione tramite {0} in corso...
                            CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_SERIAL;
                            break;
                        case 2: //RS232 simplified
                            Arca.WriteMessage(lblMain, 72, Color.White, "RS232 SIMPLIFIED"); //Tentativo di connessione tramite {0} in corso...
                            CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_SERIAL_EASY;
                            break;
                        case 3: //USB
                            Arca.WriteMessage(lblMain, 72, Color.White, "USB"); //Tentativo di connessione tramite {0} in corso...
                            CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB;
                            break;
                        case 4: //USB simplified
                            Arca.WriteMessage(lblMain, 72, Color.White, "USB SIMPLIFIED"); //Tentativo di connessione tramite {0} in corso...
                            CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB_EASY;
                            break;
                    }
                    myDll.CMDisconnect();
                    myDll.CMConnect(ref CmConnParam);
                    result = myDll.CMSingleCommand("T,1,0,88", 4);
                    i += 1;
                }
            }

            mySystem.protCassetteNumber = Convert.ToInt16(myDll.commandSingleAns[8]);
            xmlLog.AddProductAttribute("cassNumProt", mySystem.protCassetteNumber.ToString());
            result = "PASS";
            dgTest.ClearSelection();
            dgTest.Refresh();
            
            if (result != "PASS") { goto endCQA; }
            for (int i = 0; i < testList.Count(); i++)
            {
                dgTest.Rows[i].DefaultCellStyle.BackColor = Color.CornflowerBlue;
                dgTest.Rows[i].Cells[2].Value = "IN PROGRESS";
                dgTest.FirstDisplayedScrollingRowIndex = i;
                switch (testList[i].function.ToUpper())
                {
                    case "EMPTYCASSETTE":
                        result = EmptyCassette(i);
                        break;
                    case "CD80TRANSFER":
                        result = CD80Transfer(i, testList[i].parameter);
                        break;
                    case "EVOBUTTONTEST":
                        result = EvoButtonTest(i);
                        break;
                    case "TOUCHVERIFY":
                        result = TouchVerify(i);
                        break;
                    case "LAMPSBUTTONTEST":
                        result = LampsButtonTest(i);
                        break;
                    case "GETCASHDATA": 
                        dgTest.Rows[i].Cells[1].Value = "0";
                        result = GetCashDataTest(i,testList[i].parameter);
                        break;
                    case "FWCONTROL":
                        dgTest.Rows[i].Cells[1].Value = ReadDb("SELECT cod_fw FROM tblman WHERE id_prodotto ='" + mySystem.partNumber + "' AND id_cliente = " + mySystem.clientID + " AND id_modulo = 'fw_prodotto'", "cod_fw");
                        result = FwControl(i);
                        if (result!= "PASS")
                        {
                            Arca.WriteMessage(lblMain, 222, Color.LightYellow, dgTest.Rows[i].Cells[1].Value, dgTest.Rows[i].Cells[2].Value); //FW non corretto. Atteso il codice {0}, ma installato il codice {1}. Premere "C" per continuare comunque o un qualsiasi tasto per uscire
                            if (Arca.WaitingKey()== "C")
                                result = "PASS";
                        }
                        break;
                    case "SERIALSCONTROL":
                        result = SerialsControl(i);
                        break;
                    case "UNITCONFIGCONTROL":
                        result = UnitConfigControl(i);
                        break;
                    case "DATETIMECONTROL":
                        dgTest.Rows[i].Cells[1].Value = DateTime.Now.ToShortTimeString();
                        if (result != "PASS") { goto endCQA; }
                        break;
                    case "ERRORLOGREAD":
                        result = ErrorLogRead(i, testList[i].parameter);
                        break;
                    case "ERRORLOGSAVE":
                        result = ErrorLogSave(i, testList[i].parameter);
                        break;
                    case "ERRORLOGINIT":
                        result = ErrorLogInit(i);
                        break;
                    case "DEPOSIT":
                        result = Deposit(i, testList[i].parameter);
                        break;
                    case "WITHDRAWAL":
                        result = Withdrawal(i, testList[i].parameter);
                        break;
                    case "SKEWCONTROL":
                        result = SkewControl(i);
                        break;
                    case "BAGOPERATION":
                        result = BagOperation(i, testList[i].parameter);
                        break;
                    case "BAGTRANSFER":
                        result = BagTransfer(i, testList[i].parameter);
                        break;
                    case "PHOTOFEEDERTEST":
                        result = PhotoFeederTest(i);
                        break;
                    case "GETEXTENDEDSTATUS":
                        result =GetExtendedStatus(i, testList[i].parameter);
                        break;
                    case "OPENLIGHTTEST":
                        result = OpenLightTest(i);
                        break;
                }
                TestResult(i, result);
                xmlLog.AddTest(testList[i].function, "result", result);
                if (result != "PASS") goto endCQA;
            }
            SearchForEUC();
            goto endCQA;

        endCQA:
            dll = myDll.CMDisconnect();
            xmlLog.AddProductAttribute("result", result);
            switch(result)
            {
                case "1059":
                    result = "CMDTIMEOUTERROR";
                    break;
            }
            Arca.LogWrite(logFileName, ",302" + result);
            Arca.LogWrite(logFileName, ",306" + DateTime.Now.ToLongTimeString()); //Time END
            Arca.LogWrite(logFileName, ",305" + DateTime.Now.Date.ToShortDateString(),true); //Data END
            Arca.SendLog("CmCQALog.log", serverLogFolder);
            connectionToolStripMenuItem.Enabled = false;
            return result;
        }

        string OpenLightTest(int rowIndex)
        {
            string result = "FAIL";
        openL:
            Arca.WriteMessage(lblMain, 40, Arca.messageColor,"L"); // Esecuzione comando OPEN {0}
            result = myDll.CMSingleCommand("O,1,L,123456", 3, 1000);
            if (result != "1" && result != "101")
            {
                Arca.WriteMessage(lblMain, 228, Arca.errorColor, "L"); // Comando OPEN {0} FALLITO. Premi un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto openL;
            }
            Arca.WriteMessage(lblMain, 91, Arca.warningColor, "SINISTRA", "S", "DESTRA"); // Verifica che si sia accesa la luce di prenotazione di {0}. Premi {1} per confermare o ESC se è spenta o se si è accesa la luce di {2}
            aspettaL:
            switch (Arca.WaitingKey())
            {
                case "ESC":
                    goto end;
                case "S":
                case "s":
                    break;
                default:
                    goto aspettaL;
            }
            Arca.WriteMessage(lblMain, 39, Arca.messageColor, "L"); //Esecuzione comando CLOSE { 0}
            result = myDll.CMSingleCommand("C,1,L", 3, 1000);

            openR:
            Arca.WriteMessage(lblMain, 40, Arca.messageColor, "R"); // Esecuzione comando OPEN {0}
            result = myDll.CMSingleCommand("O,1,R,123456", 3, 1000);
            if (result != "1" && result != "101")
            {
                Arca.WriteMessage(lblMain, 228, Arca.errorColor, "R"); // Comando OPEN {0} FALLITO. Premi un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto openR;
            }
            Arca.WriteMessage(lblMain, 91, Arca.warningColor, "DESTRA", "D", "SINISTRA"); // Verifica che si sia accesa la luce di prenotazione di {0}. Premi {1} per confermare o ESC se è spenta o se si è accesa la luce di {2}
        aspettaR:
            switch (Arca.WaitingKey())
            {
                case "ESC":
                    goto end;
                case "D":
                case "d":
                    break;
                default:
                    goto aspettaR;
            }
            Arca.WriteMessage(lblMain, 39, Arca.messageColor, "R"); //Esecuzione comando CLOSE { 0}
            result = myDll.CMSingleCommand("C,1,R", 3, 1000);
            result = "PASS";

        end:
            return result;
        }

        string EmptyCassette(int rowIndex)
        {
            string result = "FAIL";
            myDll.CMOpen();
            for (int i = 0; i < mySystem.cassetteInstalled - 1; i++)
            {
                Arca.WriteMessage(lblMain, 83, Color.White, Convert.ToChar(i + 65)); //Svuotamento cassetto {0} in corso
                result = myDll.CMSingleCommand("I,1,L," + "".PadRight(4, Convert.ToChar(i+65)) + ",123456,2,100", 4, 120000);
                if (result != "1" && result != "101" && result != "205" && result != "206" && result != "10" && result != "11" && result != "208" && result != "64")
                {
                    result = "FAIL";
                    goto endEmpty;
                }
                result = "PASS";
            }
            endEmpty:
            myDll.CMClose();
            return result;
        }
        string GetExtendedStatus(int rowIndex,string rightStatus)
        {
            string[] status = rightStatus.Split(',');
            string correctStatus = "";
            int nCassettiVerificati = 0;
            string result = "FAIL";
            string reply = myDll.CMSingleCommand("E,1", 2);
            if (reply != "101" && reply != "1")
                goto end;

            //1-Feeder 2-Controller 3-Reader 4-Safe A/L-Cassettes
            for (int i = 3; i<myDll.commandSingleAns.Length; i++)
            {
                switch (myDll.commandSingleAns[i].Substring(0,1))
                {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "a":
                        correctStatus = "40";
                        break;
                    case "q":
                        correctStatus = "05";
                        break;
                    case "A":
                    case "B":
                    case "C":
                    case "D":
                    case "E":
                    case "F":
                    case "G":
                    case "H":
                    case "I":
                    case "J":
                    case "K":
                    case "L":
                        correctStatus = "05";
                        nCassettiVerificati += 1;
                        break;
                    case "X":
                        correctStatus = "00";
                        if (nCassettiVerificati != mySystem.cassettes.Count())
                            goto end;
                        //verificare che sia il primo cassetto oltre quelli installati
                        break;
                }
                //this.Text = myDll.commandSingleAns[i];
                if (correctStatus != myDll.commandSingleAns[i].Substring(1))
                    goto end;
            }

            //for (int i = 0; i < mySystem.cassettes.Count() + 3; i++)
            //{
            //    if (status[i] != myDll.commandSingleAns[i+3].Substring(1))
            //        goto end;
            //}

            result = "PASS";
        end:
            return result;
        }

        string GetCashData(string side = "L")
        {
        
            string result = "FAIL";
        //getCassNum:
        //    int cassNumber = 0;
        //    //Get CM Cassette number
        //    Arca.WriteMessage(lblMain,59);//GET CM CASSETTE NUMBER
        //    result = myDll.CMSingleCommand("T,1,0,61", 4);
        //    if (result != "101" && result!="1")
        //    {
        //        Arca.WriteMessage(lblMain, 218, Arca.errorColor,"GetCMCassetteNumber", result); //Il comando {0} non e' andato a buon fine, stato {1}. Premere un tasto per ripetere, ESC per uscire
        //        if (Arca.WaitingKey() == "ESC")
        //            goto end;
        //        goto getCassNum;
        //    }
            getCashData:
            //cassNumber = Convert.ToInt16(myDll.commandSingleAns[5]);
            //mySystem.cashData.cassette = new Arca.SCassette[cassNumber];
            Arca.WriteMessage(lblMain, 30);//GET CASH DATA
            result = myDll.CMSingleCommand("O,1," + side + ",123456", 3);
            result = myDll.CMSingleCommand("G,1," + side, 3);
            if (result != "101" && result != "1")
            {
                Arca.WriteMessage(lblMain, 218, Arca.errorColor,"GetCashData", result); //Il comando {0} non e' andato a buon fine, stato {1}. Premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto getCashData;
            }
            for (int i = 0; i<mySystem.cassetteInstalled; i++)
            {
                mySystem.cassettes[i].noteId = myDll.commandSingleAns[i * 5 + 4];
                mySystem.cassettes[i].noteNumber =Convert.ToInt16(myDll.commandSingleAns[i * 5 + 5]);
                mySystem.cassettes[i].freeCapacity =Convert.ToInt16(myDll.commandSingleAns[i * 5 + 6]);
                mySystem.cassettes[i].name = myDll.commandSingleAns[i * 5 + 7];
                mySystem.cassettes[i].enabled = myDll.commandSingleAns[i * 5 + 8];
            }
            result = "PASS";
        end:
            return result;
        }

        string CD80Transfer(int rowIndex,string side="L")
        {
            string result = "FAIL";
            string command = "";
            int bnTotal = 0;
            for (int cassId = 0; cassId <= 1; cassId++)
            {
                GetCashData();
                bnTotal = 0;
                command = "w,1," + side + ",0," + char.ConvertFromUtf32(cassId + 65);
                for (int i = 0 + mySystem.cd80Number; i < mySystem.cassetteInstalled; i++)
                {
                    bnTotal += mySystem.cassettes[i].noteNumber;
                }
                
                //if (bnTotal <= 200)
                //{
                for (int i = mySystem.cd80Number; i < mySystem.cassetteInstalled; i++)
                {
                    if (cassId == 0)
                    {
                        command += "," + "".PadLeft(4, (char)(i + 65)) + "," + mySystem.cassettes[i].noteNumber / 2;
                    }
                    else
                    {
                        command += "," + "".PadLeft(4, (char)(i + 65)) + "," + mySystem.cassettes[i].noteNumber;
                    }
                }
                goto sendCommand;
                //}

                //bnTotal = 200;
                //for (int i = mySystem.cd80TrayNumber * 2; i < mySystem.cassetteInstalled; i++)
                //{
                //    if (mySystem.cassettes[i].noteNumber <= bnTotal)
                //    {
                //        command += "," + "".PadLeft(4, (char)(i + 65)) + "," + mySystem.cassettes[i].noteNumber;
                //        bnTotal -= mySystem.cassettes[i].noteNumber;
                //    }
                //    else
                //    {
                //        command += "," + "".PadLeft(4, (char)(i + 65)) + "," + bnTotal;
                //        bnTotal = 0;
                //    }

                //}

            sendCommand:
                Arca.WriteMessage(lblMain, 60, Color.White, char.ConvertFromUtf32(cassId + 65)); //Trasferimento banconote verso il CD80 '{0}' in corso
                result = myDll.CMSingleCommand(command, 3, 400000);
                if (result != "101" && result != "1" && result != "14" && result != "210")
                {
                    result = "FAIL";
                    Arca.WriteMessage(lblMain, 218, Arca.errorColor, "TRANSFER TO CD80", result); //Il comando {0} non e' andato a buon fine, stato {1}. Premere un tasto per ripetere, ESC per uscire
                    if (Arca.WaitingKey() == "ESC")
                        goto end;
                    goto sendCommand;
                }
                command = "";
            }
            Arca.WriteMessage(lblMain, 61); //Svuotare i CD80 e richiudere vassoio e porta. Premere un tasto per continuare
            Arca.WaitingKey();
            WaitRecovery();
            myDll.CMSingleCommand("T,1,A,4");
            result = myDll.CMSingleCommand("Z,1,A,4," + myDll.commandSingleAns[5], 4, 8000);
            if (result != "101" && result != "1")
            {
                result = "FAIL";
                goto end;
            }
            myDll.CMSingleCommand("T,1,B,4");
            result = myDll.CMSingleCommand("Z,1,B,4," + myDll.commandSingleAns[5], 4, 8000);
            if (result != "101" && result != "1")
            {
                result = "FAIL";
                goto end;
            }
            GetCashData();
            myDll.CMClose("R");
            myDll.CMOpen("L");
            for (int i = mySystem.cd80TrayNumber*2; i<mySystem.cassetteInstalled;i++)
            {
                Arca.WriteMessage(lblMain, 66); //Init cassetti in corso
                result = myDll.CMSingleCommand("I,1,L,".PadRight(10, (char)(i + 65)) + ",123456,2,200",4,112000);
                if (result != "101" && result != "1")
                {
                    result = "FAIL";
                    goto end;
                }
            }
            
            result = "PASS";
        end:
            return result;
        }
        string SetCassetteNumber()
        {
            string result = "PASS";
            cboCassNumber.Visible = true;


            return result;
        }

        string BagTransfer(int rowIndex, string side = "L", string operation = "")
        {
            Arca.WriteMessage(lblMain, 56); //Trasferimento banconote dai cassetti alla busta in corso.
            string reply = "FAIL";
            int[] cashDada = new int[6];
            string[] noteId = new string[6];
            int freeEscrow = 0;
            reply = myDll.CMOpen(side);
            reply = myDll.CMSingleCommand("G,1," + side);
            if (reply == "101")
            {
                try
                {
                    cashDada[0] = Convert.ToInt16(myDll.commandSingleAns[5]);
                    cashDada[1] = Convert.ToInt16(myDll.commandSingleAns[10]);
                    cashDada[2] = Convert.ToInt16(myDll.commandSingleAns[15]);
                    cashDada[3] = Convert.ToInt16(myDll.commandSingleAns[20]);
                    cashDada[4] = Convert.ToInt16(myDll.commandSingleAns[25]);
                    cashDada[5] = Convert.ToInt16(myDll.commandSingleAns[30]);
                    noteId[0] = myDll.commandSingleAns[4];
                    noteId[1] = myDll.commandSingleAns[9];
                    noteId[2] = myDll.commandSingleAns[14];
                    noteId[3] = myDll.commandSingleAns[19];
                    noteId[4] = myDll.commandSingleAns[24];
                    noteId[5] = myDll.commandSingleAns[29];
                    freeEscrow = Convert.ToInt16(myDll.commandSingleAns[3 + 5 * mySystem.protCassetteNumber + 3]);
                }
                catch { }
            }
            else
                return reply;
            string parameters = "";
            int totbn = 0;
            for (int i = 0; i < 6; i++)
            {
                totbn += cashDada[i];
                if (totbn <= freeEscrow)
                    parameters += "," + noteId[i] + "," + cashDada[i].ToString();
            }

        transfer:
            Arca.WriteMessage(lblMain, 74);//Transfer in corso
            reply = myDll.CMSingleCommand("w,1," + side + ",0,q" + parameters,3, 112000);
            switch (reply)
            {
                case "210":
                case "101":
                    Arca.WriteMessage(lblMain, 77);//Transfer riuscito correttamente
                    reply = "PASS";
                    break;
                default:
                    Arca.WriteMessage(lblMain, 211, Arca.warningColor, reply); //Trasferimento banconote non riuscito (stato {0}). Premere un tasto per ripetere, ESC per uscire
                    switch (Arca.WaitingKey())
                    {
                        case "ESC":
                            reply = "FAIL";
                            goto end;
                        default:
                            goto transfer;
                    }
            }

        openEscrow:
            Arca.WriteMessage(lblMain, 76); //Apertura botola in corso
            reply = myDll.CMSingleCommand("M,1," + side + ",1,a",3,60000);

            switch (reply)
            {
                case "226":
                    //bag not present
                    break;
                case "101":
                    Arca.WriteMessage(lblMain, 78); //Ciclo di apertura e chiusura botola eseguito correttamente
                    reply = "PASS";
                    break;
                default:
                    Arca.WriteMessage(lblMain, 212, Arca.warningColor, Arca.messageColor, reply); //Apertura escrow non riuscita (stato {0}). Premere un tasto per ripetere, ESC per uscire
                    switch (Arca.WaitingKey())
                    {
                        case "ESC":
                            reply = "FAIL";
                            goto end;
                        default:
                            goto openEscrow;
                    }
            }

        end:
            return reply;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation">"insert" / "remove"</param>
        /// <returns></returns>
        string BagOperation(int rowIndex, string operation = "insert")
        {
            // open door
            // B,n,7,0,1 - temporary bag open door
            // Z,1,a,2,000002132132  - bag serial number
            // close door and wait for recovery
            // T,1,a,2 - get bag serial number

            //Aprire la cassaforte
            //inviare B,1,7,0,2,0 per aprire il modulo busta -> rc=OK
            //sigillare la busta se presente e rimuoverla
            //inviare T,1,a,5 per il bag switches status rc=OK e stato 09 (T,1,a,101,40, 09 ,0000,0000*
            //Set barcode bag Z,1,a,2,"barcode"  rc=OK
            //inviare T,1,a,5 per il bag switches status rc=OK e stato 40 (T,1,a,101,40, 40 ,0000,0000*
            //chiudere il modulo busta
            //get barcode bag T,1,a,2 -> rc=OK e par="barcode"
            string reply = "PASS";

            Arca.WriteMessage(lblMain, 52); // Aprire la porta della cassaforte. Premi un tasto per continuare, ESC per uscire
            if (Arca.WaitingKey() == "ESC")
            {
                reply = "FAIL";
                goto end;
            }
            if (operation == "insert")
            {
                openBagModule:
                Arca.WriteMessage(lblMain, 84); // Apri il gruppo busta
                reply = myDll.CMSingleCommand("B,1,7,0,5,0", 2);
                switch (reply)
                {
                    case "221":
                        Arca.WriteMessage(lblMain, 223); // Verifica che la porta sia aperta e che la macchina non sia in fase di recovery. Chiudere il gruppo busta e premi un tasto per ripetere, ESC per uscire
                        if (Arca.WaitingKey() == "ESC")
                            goto openBagModule;
                        goto end;
                    case "101":
                        break;
                    default:
                        Arca.WriteMessage(lblMain, 224); // Stato: {0} - {1}. Premere un tasto per ripetere, ESC per uscire.
                        if (Arca.WaitingKey() == "ESC")
                            goto openBagModule;
                        goto end;
                }
                Arca.WaitingTime(1000);

                Arca.WriteMessage(lblMain, 85); //Rimuovi l'eventuale busta presente. premi un tasto per continuare, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;

                checkBagNotPresent:
                reply = myDll.CMSingleCommand("T,1,a,5", 4, 70000);
                if (reply != "101")
                {
                    switch (reply)
                    {
                        default:
                            Arca.WriteMessage(lblMain, 224); // Stato: {0} - {1}. Premere un tasto per ripetere, ESC per uscire.
                            if (Arca.WaitingKey() == "ESC")
                                goto checkBagNotPresent;
                            goto end;
                    }
                }
                if (myDll.commandSingleAns[6] != "09")
                {
                    Arca.WriteMessage(lblMain, 225); // La busta sembra essere ancora presente. Rimuovila e premi un tasto per ripetere, ESC per uscire
                    if (Arca.WaitingKey() == "ESC")
                        goto checkBagNotPresent;
                    goto end;
                }

                setBagBarcode:
                reply = myDll.CMSingleCommand("Z,1,a,2,000123456789", 4);
                if (reply != "101")
                {
                    switch (reply)
                    {
                        case "202":
                            Arca.WriteMessage(lblMain, 226, Arca.errorColor, myDll.commandSingleAns[5], reply, "000123456789"); // Stato: {0} - {1}. Possibile tempo scaduto o matricola errata, non puo' superare il numero 004294967295 (inserita: {2}) Premere un tasto per ripetere, ESC per uscire.
                            if (Arca.WaitingKey() == "ESC")
                                goto openBagModule;
                            goto end;
                        default:
                            Arca.WriteMessage(lblMain, 224, Arca.errorColor, reply, ""); // Stato: {0} - {1}. Premere un tasto per ripetere, ESC per uscire.
                            if (Arca.WaitingKey() == "ESC")
                                goto setBagBarcode;
                            goto end;
                    }
                }

                Arca.WriteMessage(lblMain, 86); //Inserisci la busta appena registrata (sequenza:supporti posteriori, supporti anteriori e supporti liner), chiudi il modulo e premi un tasto per continuare
                Arca.WaitingKey();

                checkBagPresent:
                reply = myDll.CMSingleCommand("T,1,a,5", 4, 70000);
                if (reply != "101")
                {
                    switch (reply)
                    {
                        default:
                            Arca.WriteMessage(lblMain, 224); // Stato: {0} - {1}. Premere un tasto per ripetere, ESC per uscire.
                            if (Arca.WaitingKey() == "ESC")
                                goto checkBagPresent;
                            goto end;
                    }
                }
                if (myDll.commandSingleAns[6] == "09")
                {
                    Arca.WriteMessage(lblMain, 227, Arca.errorColor); // Busta non presente. Inseriscila correttamente (sequenza:supporti posteriori, supporti anteriori e supporti liner) e premi un tasto per continuare, ESC per uscire
                    if (Arca.WaitingKey() == "ESC")
                        goto checkBagPresent;
                    goto end;
                }
                Arca.WriteMessage(lblMain, 87); // Chiudi il trasporto inferiore, la cassaforte ed attendere la fine del recovery. Premi un tasto per continuare
                reply = "PASS";
            }
            else
            {
                unlockHandle:
                Arca.WriteMessage(lblMain, 53); // Sblocco maniglia in corso
                reply = myDll.CMSingleCommand("B,1,7,0,1", 2);
                switch (reply)
                {
                    case "101":
                        reply = "PASS";
                        break;
                    case "":
                        //Nessuna risposta
                        break;
                    //case "102":
                    //    //busy
                    //    break;
                    case "221":
                        Arca.WriteMessage(lblMain, 210, Arca.warningColor); //Stato 221 (UNEXPECTED COMMAND), verificare che la porta sia aperta. Premere un tasto per ripetere, ESC per uscire
                        switch (Arca.WaitingKey())
                        {
                            case "ESC":
                                reply = "FAIL";
                                goto end;
                            default:
                                goto unlockHandle;
                        }
                    default:
                        Arca.WriteMessage(lblMain, 208, Arca.errorColor, reply); // Sblocco maniglia non avvenuto correttamente. Stato macchina: {0}. Premere un tasto per ripetere, ESC per uscire
                        switch (Arca.WaitingKey())
                        {
                            case "ESC":
                                reply = "FAIL";
                                goto end;
                            default:
                                goto unlockHandle;
                        }
                }
                Arca.WriteMessage(lblMain, 58); //Estrarre la busta (alzare il maniglione, spingerlo fino in fondo per poter estrarre il supporto busta). Premere un tasto per continuare
            }



            //Arca.WriteMessage(lblMain, 58); //Estrarre la busta (alzare il maniglione, spingerlo fino in fondo per poter estrarre il supporto busta). Premere un tasto per continuare
            //if (operation == "insert")
            //    Arca.WriteMessage(lblMain, 57); //Inserire la busta (alzare il maniglione, spingerlo fino in fondo per poter estrarre il supporto busta). Premere un tasto per continuare
            if (Arca.WaitingKey() == "ESC")
            {
                reply = "FAIL";
                goto end;
            }


            end:
            //reply = myDll.CMSingleCommand("Z,1,a,2,000000000000", 4);
            WaitRecovery();
            dgTest.Rows[rowIndex].Cells["colValore"].Value = reply;
            return reply;
        }

        private void SearchForEUC ()
        {
            string mySQL = "SELECT DISTINCT id_prodotto, id_cliente, cdfcode FROM tblcustomization WHERE id_prodotto LIKE'" + mySystem.partNumber + "' and id_cliente = " + mySystem.clientID ;
            myConnection.Open();
            dbCommand = myConnection.CreateCommand();
            dbCommand.CommandText = mySQL;
            dbReader = dbCommand.ExecuteReader();
            xmlLog.AddProductAttribute("EUC", "NOTODO");
            while (dbReader.Read())
            {
                Arca.WriteMessage(lblMain, 43); //ATTENZIONE! Questo part number necessita della configurazione supplementare con il software END USER CUSTOMIZATION, premere 'E' per continuare
                xmlLog.AddProductAttribute("EUC", "TODO");
                if (dbReader.GetString(2) != "")
                {
                    if (dbReader.GetString(2).Substring(0,2) == "FF")
                    {
                        Arca.WriteMessage(lblMain, 90); //ATTENZIONE! Questo pn necessita la configurazione con il sw END USER CUSTOMIZATION ed è necessario installare il CDF (non montare il copri piastra), premere 'E' per continuare
                    }
                }
                
                Arca.WaitingKey("E");
            }
            myConnection.Close();
        }

        string PhotoFeederTest(int rowIndex)
        {
        start:
            string result = "FAIL";
            
            string reply = myDll.CMSingleCommand("T,1,0,0", 4);
            if (reply != "101" && reply != "1") goto end;

            if (myDll.commandSingleAns[8] != "0")
            {
                Arca.WriteMessage(lblMain, 69, Arca.warningColor); //Il cover risulta APERTO, chiuderlo correttamente e premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto start;
            }
            
            openFeeder:
            Arca.WriteMessage(lblMain, 67);//Premere il pulsante di sblocco cover dal lato del feeder e premi un tasto per continuare
            Arca.WaitingKey();
            reply = myDll.CMSingleCommand("T,1,0,0", 4);
            if (reply != "101" && reply != "1") goto end;
            if (myDll.commandSingleAns[8] != "1")
            {
                Arca.WriteMessage(lblMain,68,Arca.warningColor); //Il cover risulta chiuso, premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto openFeeder;
            }

        closeFeeder:
            Arca.WriteMessage(lblMain, 70);//Chiuere correttamete il cover e premere un tasto per continuare
            Arca.WaitingKey();
            reply = myDll.CMSingleCommand("T,1,0,0", 4);
            if (reply != "101" && reply != "1") goto end;
            if (myDll.commandSingleAns[8] != "0")
            {
                Arca.WriteMessage(lblMain, 69, Arca.warningColor); //Il cover risulta APERTO, chiuderlo correttamente e premere un tasto per ripetere, ESC per uscire
                if (Arca.WaitingKey() == "ESC")
                    goto end;
                goto closeFeeder;
            }
            result = "PASS";

        end:
            dgTest.Rows[rowIndex].Cells["colValore"].Value = result;
            Arca.LogWrite(logFileName, ",31A" + result);
            return result;

        }

        private void WaitRecovery()
        {
            int timeElapsed = 0;
            int waitTime = 70;
            Arca.WriteMessage(lblMain, 55); //Recovery in corso
            Arca.WaitingTime(15000);
            timeElapsed = 15;
            string reply = myDll.CMSingleCommand("T,1,0,13", 4, 5000);
            while (reply == "102" || reply == "4")
            {
                Arca.WaitingTime(1000);
                reply = myDll.CMSingleCommand("T,1,0,13", 4, 5000);
                timeElapsed += 1;
                if (timeElapsed > waitTime)
                {
                    Arca.WriteMessage(lblMain, 209, Arca.waitingColor); //Durata del recovery troppo lunga. Premere un tasto per attendere ancora, ESC per uscire
                    if (Arca.WaitingKey() == "ESC")
                        return;
                    waitTime += 10;
                }
            }
        }
        

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Arca.keyCodePressed = e.KeyValue;
        }

        private void txtSerialNumber_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtSerialNumber.TextLength==12)
            {
                txtSerialNumber.Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CMClose("R");
            CMClose("L");
            myDll.CMDisconnect();
            Application.Exit();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        void MakeLocalDBCopy()
        {
            DirectoryInfo di = Directory.CreateDirectory(localDBFolder);
            Arca.WriteMessage(lblMain, 73);//Copia DB in locale in corso...
            Application.DoEvents();
            try
            {
                File.Copy(Path.Combine(serverDBFolder, databaseName), Path.Combine(localDBFolder, databaseName), true);
            }
                catch(Exception ex)
            {
                if (ex.HResult != -2147024816)
                {
                    Arca.WriteMessage(lblMain, 221, Color.Orange, ex.Message);//. Premere un tasto per continuare
                    MessageBox.Show(ex.Message);
                    //Arca.WaitingKey();
                }

            }
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string myMessage = "Connection OK";
            if (mySystem.family.ToUpper() == "OM61" || mySystem.family.ToUpper() == "CM18SOLO" || mySystem.family.ToUpper() == "CM18SOLOT")
                CmConnParam.ConnectionMode = CmDLLs.DLINK_MODE_USB;
            int dll = myDll.CMConnect(ref CmConnParam);
            if (dll != 0)
                myMessage = "Connection error: " + dll;
            MessageBox.Show(myMessage);
            //commandToolStripMenuItem.Enabled = true;
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string myMessage = "Disconnect OK";
            int dll = myDll.CMDisconnect();
            if (dll != 0)
                myMessage = "Disconnect error: " + dll;
            MessageBox.Show(myMessage);
            //commandToolStripMenuItem.Enabled = false;
        }

        private void shortStatisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string message = lblMain.Text;
            Arca.WriteMessage(lblMain, 79); //Attivazione modalità transparent
            myDll.CMTransparentIn();
            Arca.WriteMessage(lblMain, 81); //Richiesta versione fw lettore in corso
            myDll.CMTransparentCommand("B3040F0F02");
            string answer = myDll.commandAns;
            Arca.WriteMessage(lblMain, 82); //Richieste statistiche ultimo deposito in corso
            if (answer.Substring(1,1)=="0")
                myDll.CMTransparentCommand("D30621", 50);
            else
                myDll.CMTransparentCommand("D3060F",50);
            answer = myDll.commandAns;
            Arca.WriteMessage(lblMain, 80); //Disativazione modalità transparent
            myDll.CMTransparentOut();
            ShortStatistics frmStatistics = new ShortStatistics(answer);
            frmStatistics.ShowDialog();
            lblMain.Text = message;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            //MakeLocalDBCopy();
            //FormInizialize();
        }

        private void tianmaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Z,n,6,30, dispID
            
            myDll.CMSingleCommand("Z,1,6,30,4");

        }

        private void hitachiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Z,n,6,30, dispID

            myDll.CMSingleCommand("Z,1,6,30,5");
        }

        private void dLCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Z,n,6,30, dispID

            myDll.CMSingleCommand("Z,1,6,30,6");
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void openLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myDll.CMSingleCommand("O,1,L,123456");
        }

        private void splMain_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void errorLogToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ErrorLogRead(0, "NO43664");
        }
    }
}
