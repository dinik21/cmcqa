CM CQA TEST

┌────────┬──────────────┬───────────────────────────┐
│Version │ Release Date │ Release Type              │
├────────┼──────────────┼───────────────────────────┤
│ 1.0.1  │  01/02/2018  │ Added Features            │
├────────┼──────────────┼───────────────────────────┤
│ 1.0.0  │  30/11/2018  │ First release             │
├────────┼──────────────┼───────────────────────────┤
│        │              │                           │
├────────┼──────────────┼───────────────────────────┤
│        │              │                           │
└────────┴──────────────┴───────────────────────────┘

Version 1.0.1 01/02/2017 ECR N°
- Added Cassette codes managment
	Added Cassette codes control during the SN test. The good codes are registrered in the config.ini file.
- Modified system family managment
	Improve the system family managment reading the DBFW description and dividing family,CRM and CD80 fields
- Added log file save on the server
	At the end of the test, the log file will be merged with the server log file
- Modified files:
	CmCQA.exe
	italiano.lang
	config.ini
	
Version 1.0.0 30/11/2017
- First Release 